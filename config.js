// Variable globale pour prodigeviewerapi
var CARMEN_URL_SERVER_DATA = null;

/*** Configuration des constante de l'application */
var appConfig = (function(){
  'use strict';
  var _const = {
    ////////////////////////// Config commun avec la partie services php //////////////////////////////

    /** Parcel */
    numChampName:    null,
    niveauChampName: null,
    idChampName:     null,
    villeChampName:  null,

    /** Context de la carte */
    context    : "services/getContext.php",
    mapfile    : null,
   
    /** Paramètre Api Prodige */
    urlProdige : null,
    urlMetadataInfo: null,

    ////////////////////////// Config javascript //////////////////////////////

    /** url requette prodige */
    getArea    : '/frontcarto/query/getareas',
    getInformation :  '/frontcarto/query/getinformation',
    getAdvancedAttributes: '/frontcarto/query/advancedqueryattributes',

    /** Export Pdf*/
    exportPdfUrl: "services/exportPdf.php",

    /*** Proxy */
    proxy: 'services/proxy.php',
    proxyParam: 'proxy_url'

  } 

  return {
    /**
     * @param {string} param 
     */
    get: function (param){
      return _const[param];
    },

    updateFromServer: function(){
      var defer = $.Deferred();
      var promise = request.get('services/getConfig.php');

      promise.then(
        function(response){
          try {
            var config = JSON.parse(response);

            Object.keys(config).forEach(function(key){
              _const[key] = config[key];
            });

            if(_const.urlProdige){
              ['getArea', 'getInformation', 'getAdvancedAttributes' ].forEach(
                function(key) {
                  _const[key] = _const.urlProdige + _const[key];
                }
              )
            }

            CARMEN_URL_SERVER_DATA = config['URL_DATACARTO_PRODIGE'] ? config['URL_DATACARTO_PRODIGE'] : URL_DATACARTO_PRODIGE;
            Prodige.prodigeServerVersion = config['PRODIGE_API_VERSION'] ? config['PRODIGE_API_VERSION'] : '4.1';
            Prodige.mapfile = _const['mapfile'];
            console.info('Prodige Api Version : ',Prodige.prodigeServerVersion );

            defer.resolve('ok');
          } catch (error) {
            console.error(error);
            defer.reject('ko');
          }
          
        },
        function(error){
          console.error(error);
          defer.reject('ko');
        }
      );

      return defer.promise();
    }
  }
})();
