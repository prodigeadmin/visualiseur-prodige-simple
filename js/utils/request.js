var request = (function(){
  'use strict'

  /**
   * 
   * @param {string} url 
   * @param {Array} params 
   */
  function getByProxy(url, params){
    var queryParam = $.param(params);

    var proxyUrl = url + "?" + queryParam;
    var data = {};
    data[ appConfig.get('proxyParam') ] = proxyUrl;

    return $.ajax({
      url: appConfig.get('proxy'),
      data: data
    })
  }

  /**
   * 
   * @param {string} url 
   * @param {Array} params 
   */
  function postByProxy(url, params){
    var proxyUrl = appConfig.get('proxyParam') + "=" + url;

    return $.ajax({
      url: appConfig.get('proxy') + "?" + proxyUrl,
      data: params,
      type: "POST"
    })
  }

  /**
   * 
   * @param {string} url 
   * @param {Array.<string | number} param 
   */
  function get(url, param, header){
    var config = {
      url: url,
      data: param,
      type: "GET",
      headers: header
    };

    if(param){
      config.param  = param;
    }

    if(header){
      config.header = header;
    }

    return $.ajax(config);
  }

  /**
   * 
   * @param {string} url 
   * @param {Array.<string | number} param 
   */
  function post(url, param){
    return $.ajax({
      url: url,
      data: param,
      type: "POST"
    })
  }

  return{
    getByProxy: getByProxy,
    postByProxy: postByProxy,
    get: get,
    post: post
  }

})();