var mapColor = (function(){
  'use strict';
  /** */
  var _colorLayer = null;

  /** */
  var _promise = null;

  function getLayerColor(){
    if(!_colorLayer){
      if(!_promise){
        _promise = $.Deferred();
         
        request.get("services/mapColor.php").then(
          function(resultJson){
            try {
              _colorLayer = JSON.parse(resultJson);

              _promise.resolve(_colorLayer);
            } catch (error) {
              _promise.reject(error);
              console.error(error);
            }
          },
          function(error){
            _promise.reject(error);
            console.error(error);
          }
        )
      }
      
      return _promise.promise();
    }
    else{
      var d = $.Deferred();

      d.resolve(_colorLayer);

      return d.promise();
    }
  }

  return {
    getLayerColor: getLayerColor
  }
})();
