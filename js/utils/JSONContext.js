(function(){
  'use strict'

  /*********************************************************************************************
 * Context Class definition
 *********************************************************************************************/   
  Carmen.JSONContext = new OpenLayers.Class({
    jsOws : null,
    generalExt : null,
    layer : [],
    mapProjection : null,
    
    mdataTool : null,
    mdataMap : null,
    mdataLayer : null,
    mapExtent : null,
    maxExtent : null,
    mapMinScaleDenom : null,
    mapMaxScaleDenom : null,

    initialize: function(context) {
      this.jsOws = context;
      this.generalExt = this.jsOws.ViewContext.General.Extension; 
      if (this.jsOws.ViewContext.LayerList && this.jsOws.ViewContext.LayerList.Layer) {
      this.layer = this.jsOws.ViewContext.LayerList.Layer instanceof Array ?
        this.jsOws.ViewContext.LayerList.Layer :
        [].concat(this.jsOws.ViewContext.LayerList.Layer);
      this.jsOws.ViewContext.LayerList.Layer = this.layer
      }
      this.parseMdata();
    },
    
    getObj : function () {
      return this.jsOws;  
    },
    
    getProjection :  function() {
      if (this.mapProjection==null)
        this.mapProjection= this.jsOws.ViewContext.General.BoundingBox.attributes.crs.split('=')[1].toLocaleUpperCase();
      
      return this.mapProjection;
    },
    
    getProjectionUnits : function() {
      if (this.mapProjectionUnits==null)
        this.mapProjectionUnits= Carmen.Util.getProjectionUnits(this.getProjection());
      return this.mapProjectionUnits;
    },
    
    getExtent :  function() {
      if (this.mapExtent==null) {
        this.mapExtent = Carmen.Util.convertContextBBtoolBounds(
          this.jsOws.ViewContext.General.BoundingBox);
      }
      return this.mapExtent;
    },
    
    getMaxExtent :  function() {
      if (this.maxExtent==null) {
        this.maxExtent = Carmen.Util.convertContextBBtoolBounds(
          this.jsOws.ViewContext.General.Extension.MaxBoundingBox);
      }
      return this.maxExtent;
    },
    
    getMaxScaleDenom : function() {
        if (this.mapMaxScaleDenom==null && 
            this.jsOws.ViewContext.General.Extension.MaxScaleDenom!=null) {
          this.mapMaxScaleDenom = parseFloat(this.jsOws.ViewContext.General.Extension.MaxScaleDenom);
          if (this.mapMaxScaleDenom ==-1) 
            this.mapMaxScaleDenom=null;
        }
        return this.mapMaxScaleDenom;
    },
    
    getMinScaleDenom : function() {
        if (this.mapMinScaleDenom==null && 
            this.jsOws.ViewContext.General.Extension.MinScaleDenom!=null) {
          this.mapMinScaleDenom = parseFloat(this.jsOws.ViewContext.General.Extension.MinScaleDenom);
          if (this.mapMinScaleDenom == -1)   
            this.mapMinScaleDenom=null;
        }
        return this.mapMinScaleDenom;
    },
    
    
    getTitle :  function() {
      return Url.decode(this.jsOws.ViewContext.General.Title);
    }, 

    getFavoriteAreas : function() {
      return this.generalExt.GeoBookmark ? this.generalExt.GeoBookmark : null;
    },
    
    setExtent : function(bounds) {
      this.jsOws.ViewContext.General.BoundingBox.attributes.minx = bounds.left;
      this.jsOws.ViewContext.General.BoundingBox.attributes.miny = bounds.bottom;
      this.jsOws.ViewContext.General.BoundingBox.attributes.maxx = bounds.right;
      this.jsOws.ViewContext.General.BoundingBox.attributes.maxy = bounds.top;
    },
    
    setLayerVisibility : function(layerIdx, visible) {
      this.jsOws.ViewContext.LayerList.Layer[layerIdx].attributes.hidden = visible ? "0" : "1";
    },
    
    setGeneralConfig : function(config) {
      for(p in config) 
      this.generalExt[p] = config[p];
    },
    
    getGeneralConfig : function(configName) {
      var config = null;
      if (configName in this.generalExt)
        config = this.generalExt[configName];
      return config;
    },
    
    isToolActiv : function(toolName) {
      var toolValue = Ext.valueFrom(this.mdataTool[toolName], "0") 
      return (toolValue=="1" || toolValue=="ON" || toolValue== "simple"|| toolValue=="advanced");
    },
    
    getToolbarTarget : function(toolName) {
      var toolValue = Ext.valueFrom(this.mdataTool[toolName], "0") 
      return toolValue;
    },

    hasLayerWMSC : function() {
      var found = false;
      for (var k in this.mdataLayer) {
        if (Ext.valueFrom(this.mdataLayer[k].wmscLayer, "OFF", false)=="ON"
          && Ext.valueFrom(this.mdataLayer[k].isWmts, "NO", false)=="NO" ) {
          found = true;
          break;
        }
      }
      return found;
    }, 

    // fill hash mdataTool, mdataMap and mdataLayer 
    // with mdata from Extension parts of the context
    parseMdata : function () {
      this.mdataTool = {};
      this.mdataMap = {};
      for (var k in this.generalExt) {
        var matched = k.match(/^tool_(.+)$/); 
        if (matched!=null) {
          var toolName = matched[1];
          this.mdataTool[toolName] = this.generalExt[k];
        }
        else {
          matched = k.match(/^mapSettings_(.+)$/);
          if (matched!=null) {
            var setting = matched[1];
            this.mdataMap[setting] = this.generalExt[k];
          }
          else
            this.mdataMap[k] = this.generalExt[k];
        }
      }
      this.mdataLayer = {};
      if (this.layer) {
        if (this.layer instanceof Array) {
          for (var i=0; i<this.layer.length; i++) {
            var layerName =  this.layer[i].Title;
            this.mdataLayer[layerName] = {};
            for (var k in this.layer[i].Extension) {
              var matched = k.match(/^layerSettings_(.+)$/);
              var setting = matched!=null ? matched[1] : k;
              this.mdataLayer[layerName][setting] = this.layer[i].Extension[k];     
            }
          }
        }
        else {
          var layerName =  this.layer.Title;
          this.mdataLayer[layerName] = {};
          for (var k in this.layer.Extension) {
            var matched = k.match(/^layerSettings_(.+)$/);
            var setting = matched!=null ? matched[1] : k;
            this.mdataLayer[layerName][setting] = this.layer.Extension[k];     
          } 
        }
      }
    }, 
    
    
    getNextLayerId : function() {
      return this.layer.length;
    },
    
    addLayerDescs :  function(descs) {
      for (var i=0; i< descs.length; i++)
        this.layer.push(descs[i]);
      return (this.layer.length-1);
    },
    
    getLayerDescByName : function(value) {
      var i=0;
      for (var k in this.layer) {
        var v = Ext.valueFrom(Url.decode(this.layer[k].Extension.LAYER_ID), null, false);
        if (v==value) {
          break;
        }
        i++;
      }
      return i <= (this.layer.length-1) ? this.layer[i] : null;
    },
    
    getLayerDescFromMdataValue : function(attributeName, value) {
      var i=0;
      for (var k in this.mdataLayer) {
        var v = Ext.valueFrom(Url.decode(this.mdataLayer[k][attributeName]), null, false);
        if (v==value) {
          break;
        }
        i++;
      }
      return i <= (this.layer.length-1) ? this.layer[i] : null;
    }
  });
  
  Carmen.JSONContext.CLASS_NAME = "Carmen.JSONContext";
})();