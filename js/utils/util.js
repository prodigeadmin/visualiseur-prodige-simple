var util = (function(){
  'use strict'

  function urldecode(url) {
    return decodeURIComponent(url.replace(/\+/g, ' '));
  }

  /**
   * 
   * @param {Array} arr 
   */
  function uniq(arr){
    var retour = [];
    if(arr && arr instanceof Array ) {
      retour = arr.filter(function(elem, index, self) {
        return ( index === self.indexOf(elem) );
      });
    }

    return retour; 
  }
  
  function checkElement(element, id){
    var isOk = true;
    if(!element){
      console.error("Aucun élement html avec l'id :", id);
      isOk = false;
    }

    return isOk;
  }
  /**
   * @param  {Object} data
   * @param  {Array.<string>} path
   * 
   * @return {any}
   */
  function getValueWithPath(data, path){
    var isOk = true;
    var _data = JSON.parse(JSON.stringify(data)); // copy sans reference

    if(path && path instanceof Array){
      path.forEach(
        function(key){
          if(isOk && _data[key]){
            _data = _data[key];
          }
          else{
            isOk = false;
          }
        }
      )
    }


    return isOk ? _data : null;
  }
  return {
    urldecode: urldecode,
    uniq: uniq,
    checkElement: checkElement,
    getValueWithPath: getValueWithPath
  }
})();