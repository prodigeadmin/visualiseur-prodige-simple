var generateCtrlConfig = (function(){
  'use strict'

  /** */
  function toZoom(context){
    var combosConfig = Carmen.Util.chompAndSplit( util.urldecode(context.mdataTool.zoomToPlaceConfig) ,';');
    
    combosConfig.forEach(function(config, i){
      combosConfig[i] = Carmen.Util.chompAndSplit(config,'|'); 
    })
   
    var ctrlZoomConfigs = [];
    
    combosConfig.forEach(function(config, i){
      var configCtrl = {
        labelText : config[0], 
        fieldNameId:  config.length === 7 ? config[6] : null,
        providerUrl: appConfig.get('getArea') ,
        providerBaseParams: { 
          'map' : window.mapfile, 
          'layer':  config[1], 
          'fieldId': config[2], 
          'fieldName': config[3],
          'fieldEchelle': config[4]
        }
      };

      if (config.length>5) {
        configCtrl['linkedCombo'] = config.length === 7 ? config[5] : null ;
        configCtrl['linkedFieldName'] = 'filter_id';
        configCtrl['providerLinkParams'] = { 'filter_item': combosConfig[ config.length === 7 ? 6 : 5], 'filter_id' : '' };
      }

      ctrlZoomConfigs.push(configCtrl);
    });

    return ctrlZoomConfigs;
  }

  return{
    toZoom: toZoom
  }
})() 
 