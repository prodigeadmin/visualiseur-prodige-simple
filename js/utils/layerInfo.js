var layerInfo = (function(){
  'use strict';



  function updateSidebar(result, sidebar, layer){
    var title = "";
    var path = ["metadata", "title"];
    var pathAbstract = ["metadata","abstract"];

    var title = util.getValueWithPath(result, path);
    var abstract = util.getValueWithPath(result, pathAbstract);
    var name = util.getValueWithPath(layer, ['attributes','name']);

    if(name){
      sidebar.updateLayerInfo(name, {title: title, abstract: abstract});
    }
  }

  function getRequest(context, sidebar){
    var isOk = true;
    isOk = isOk && (context && context.layer instanceof Array)
    isOk = isOk && !!sidebar;

    if(!isOk){
      console.error("Erreur avec les paramètres");
      return false;
    }

    var _layers = context.layer;

    _layers.forEach(function(layer){
      if(layer.Extension && layer.Extension.GEONETWORK_METADATA_ID){
        var inProd = true;
        var forProd = inProd ? layer.Extension.GEONETWORK_METADATA_ID : '8c146fc8-8683-43e1-aeed-16e91ebdb32e';
        
        if(!inProd){
          console.error("UTILISATION DU MODE DEV dans layerInfo.getRequest");
        }
        
        var param = {_content_type: 'json',fast: 'index', uuid: forProd };

        request.get(appConfig.get('urlMetadataInfo'), param)
        .then( 
          function(result){ 
            updateSidebar(result, sidebar, layer)
          }
        ).catch(
          function(error){
            console.error(error);
          }
        );
      }
    });

    return true;
   
    
  }


  return{
    getRequest: getRequest
  }
})();