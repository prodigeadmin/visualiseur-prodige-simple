var Sidebar = (function(){
  'use strict'

  /**
   * 
   * @param {string} sidebarElementId 
   * @param {string} mapElementId 
   */
  function _contructor(sidebarElementId, prodigeMap, layerColor){
    var self = this; 

    var _sidebar = null;
    var _prodigeMap = null;
    var _sidebarCollapse = null;
    var _sidebarContent = null;
    var _divLayerBadge = [];
    var _layerColor = [];
    var _parcel = null;
    var _niveaux = {};
    var _commune_info = {};
    var _abstracts = {};
    var _documents ={};

    Object.defineProperty(this, "sidebar",{
      get: function() {
        return _sidebar; 
      },
      enumerable: true
    });

    Object.defineProperty(this, "sidebarCollapse",{
      get: function() {
        return _sidebarCollapse; 
      },
      enumerable: true
    });

    Object.defineProperty(this, "prodigeMap",{
      get: function() {
        return _prodigeMap; 
      },
      enumerable: true
    });

    Object.defineProperty(this, "sidebarContent",{
      get: function() {
        return _sidebarContent; 
      },
      enumerable: true
    });

    Object.defineProperty(this, "divLayerBadge",{
      get: function() {
        return _divLayerBadge; 
      },
      set: function(divLayerBadge) {
        if(divLayerBadge && divLayerBadge instanceof Array){
          _divLayerBadge = divLayerBadge;
        }
      },
      enumerable: true
    });

    Object.defineProperty(this, "layerColor",{
      get: function() {
        return _layerColor; 
      },
      set: function(layerColor) {
        if(layerColor && layerColor instanceof Object){
          _layerColor = layerColor;
        }
      },
      enumerable: true
    });

    Object.defineProperty(this, "parcel",{
      get: function() {
        return _parcel; 
      },
      set: function(parcel) {
        if(parcel && parcel instanceof Parcel){
          _parcel = parcel;
        }
      },
      enumerable: true
    });

    Object.defineProperty(this, "niveaux",{
      get: function() {
        return _niveaux; 
      },
      set: function(niveaux) {
        if(niveaux && niveaux instanceof Object){
          _niveaux = niveaux;
        }
      },
      enumerable: true
    });
    
    Object.defineProperty(this, "commune_info",{
      get: function() {
        return _commune_info; 
      },
      set: function(commune_info) {
        if(commune_info && commune_info instanceof Object){
          _commune_info = commune_info;
        }
      },
      enumerable: true
    });
    
    Object.defineProperty(this, "abstracts",{
      get: function() {
        return _abstracts; 
      },
      set: function(niveaux) {
        if(abstracts && abstracts instanceof Object){
          _abstracts = abstracts;
        }
      },
      enumerable: true
    });
    
    Object.defineProperty(this, "documents",{
      get: function() {
        return _documents; 
      },
      set: function(documents) {
        if(documents && documents instanceof Object){
          _documents = documents;
        }
      },
      enumerable: true
    });
  
    _prodigeMap = prodigeMap;
    _sidebar = $('#' + sidebarElementId );
    _sidebarCollapse = $('#' + sidebarElementId + 'Collapse');
    _sidebarContent = $('#' + sidebarElementId + 'Content');

    self.layerColor = layerColor;

    if( $('#sidebar-info') ){
      $('#sidebar-info').hide();
    }
    
    if($('#sidebar-pdf')){
      $('#sidebar-pdf').hide();
    }
    
    if( util.checkElement(_sidebar, sidebarElementId ) && util.checkElement(_sidebarCollapse, sidebarElementId + 'Collapse' ) +util.checkElement(_sidebarContent, sidebarElementId + 'Content' )  ){
     
      _sidebarCollapse.on('click', function () {
        self.togle();
      });
    }
  }

  function _drawMainLayer(layer){
    var self = this;
    var section = null;

    if(self && layer &&  self.sidebarContent){
      section = document.createElement("section");

      var title =  document.createElement("h3");
      title.innerHTML = layer.text;
      $(title).addClass('section-title');

      section.appendChild(title);

      self.sidebarContent.append(section);
    }

    return section;
  }

  function _drawChildNode(section, nodes){
    var self = this;
    if(self && section && nodes && nodes instanceof Array ){
      var ul = document.createElement('ul');
      $(ul).addClass('list-unstyled');
      
      var map = self.prodigeMap.map;
      nodes.forEach(function(node){
        if(node.attributes.displayClass==="1"){
          var li = document.createElement('li');
          $(li).addClass('row')  

          var div = document.createElement('div');
          $(div).addClass('checkbox');

          var label = document.createElement('label');

          var divForSpan = document.createElement('div');
          var divForInput = document.createElement('div');

          var input = $('<input type="checkbox" data-toggle="toggle" data-toggle="toggle" data-size="mini">');

          var icon = 	$('<i id="icon-'+node.attributes.layerName+'" class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Message d\'information"></i>');

          divForSpan.id = 'divForSpan-' + node.attributes.layerName  ;

          self.divLayerBadge.push($(divForSpan));

          var divText = document.createElement('div');
          var spanTitle = document.createElement('span');
          spanTitle.id = "span-title-" + node.attributes.layerName;

          $(divText).addClass('col-xs-7');
          $(spanTitle).append(node.text+ " ")
          $(divText).append(spanTitle);

          $(divText).on("click", function(){
            self.showAbstract(node.attributes.layerName);
          });

          section.appendChild(ul);

          $(divForSpan).addClass('col-xs-2');

          $(divForInput).addClass(['col-xs-3']);
          $(divForInput).append(input);


          $(label).append( divText, divForSpan ,divForInput );

          div.appendChild(label);

          li.appendChild(div);

          ul.appendChild(li);


          if(node.checked){
            input.bootstrapToggle('on');
          }else{
            input.bootstrapToggle('off');
          }
          
          input.change(function(){
            self.onToggle(node, input.prop('checked'));
          });
        }
        
        node.attributes.OlLayer.setOpacity(node.attributes.opacity);

      });

     
    }
  }

  /////////////// Fonction ///////////////////////////

  
  /** */
  _contructor.prototype.open = function(){
    var self  = this;
    self.sidebar.addClass('active');
    $('body').addClass('sidebar-active');
    self.sidebarCollapse.addClass('navigation-toggle-closer');
  }

  /** */
  _contructor.prototype.close = function(){
    var self  = this;
    self.sidebar.removeClass('active');
    $('body').removeClass('sidebar-active');
    self.sidebarCollapse.removeClass('navigation-toggle-closer');
  }

  /** */
  _contructor.prototype.showAbstract = function(layerName){
    var self = this;
    if(self.abstracts[layerName] && $('#modal-layer-absract')){
      var info =  self.abstracts[layerName];
     
      if( $("#modal-layer-absract-title") && info  && info.title ){
        $("#modal-layer-absract-title").text( info.title );
      };

      if( $("#modal-layer-absract-content") && info  && info.abstract ){
        $("#modal-layer-absract-content").text( info.abstract );
      }; 
      
      $('#modal-layer-absract').modal('show');
    }
  }

  /** */
  _contructor.prototype.togle = function(){
    var self  = this;
    if( $('body').hasClass('sidebar-active') ){
      self.close();
    }else{
      self.open();
    }
    self.prodigeMap.map.updateSize();
  }

  /** */
  _contructor.prototype.updateTreeLayer = function(){
    var self = this;
    var layerTree =  self.prodigeMap.layerTreeManager.getLayerTree();
    if(layerTree && layerTree.childNodes && layerTree.childNodes instanceof Array){
      self.divLayerBadge = [];
      layerTree.childNodes.forEach(function(layer){
        var section = _drawMainLayer.call(self, layer );
        _drawChildNode.call(self, section, layer.childNodes );
      })
    }
  }

  /** */
  _contructor.prototype.onToggle = function(layer, checked){
    var node = layer.attributes.OlLayer;
    node.setOpacity(checked ? (layer.attributes.opacity) : 0);
  }

  /**
   * 
   */
  _contructor.prototype.updateNiveaux = function(layers){
    var self = this;
    self.niveaux = {};
    self.divLayerBadge.forEach(function(div){
      div.empty();
    });

    layers.forEach(function(layer){
      if( $('#divForSpan-' + layer.layer) ){
        var el =  $('#divForSpan-' + layer.layer);

        el.empty();
        
        self.niveaux[layer.layer] = util.uniq(layer.values);
        //self.layerColor[layer.layer].forEach(function(text){

        for(var text in self.layerColor[layer.layer]){
        //self.niveaux[layer.layer].forEach(function(value){
          if(self.niveaux[layer.layer].indexOf(text)!=-1){
          //var color = ( self.layerColor && self.layerColor[layer.layer] && self.layerColor[layer.layer][value] ) ? self.layerColor[layer.layer][value] : '#777';
            var color = self.layerColor[layer.layer][text];
            el.append( $('<span class="badge pull-right" style="background : '+ color +'" >'+text+'</span>') );
          }
        }
      }
    });
  } 
  
  /**
   * @param {Parcel} parcel
   */
  _contructor.prototype.updateParcel = function(parcel){
    var self = this;
    self.parcel = parcel;
    if($('sidebar-info') && $('#sidebar-parcel-name') && $('#sidebar-commune-name') && parcel instanceof Parcel){
      
      $('#sidebar-parcel-name').text( "Parcelle " + parcel.section + parcel.numParcel );
      $('#sidebar-commune-name').text( "Commune : " + parcel.city );
      
    }
  }
  /**
   * @param {Parcel} parcel
   */
  _contructor.prototype.updateCommuneInfo = function(commune_info){
    var self = this;
    self.commune_info = commune_info;
  }
  
  /**
   * @param {Parcel} parcel
   */
  _contructor.prototype.updateDocuments = function(documents){
    var self = this;
    self.documents = documents;
    
    if($('#documents')){
      $('#documents').remove();
    }
    
    
    if(documents){
      var section = document.createElement("section");
      section.id = "documents";    


      var title =  document.createElement("h3");
      title.innerHTML = "Documents";
      $(title).addClass('section-title');
      section.appendChild(title);
      var ul = document.createElement('ul'); 
      
      for(var i in documents){
        var li = document.createElement('li'); 
        var a = document.createElement('a'); 
        a.href = documents[i];
        a.target = '_blank';
        if((appConfig.get('tabReglement')[i]!==undefined)){
          a.innerHTML = "<i class=\"fas fa-file-pdf\"></i> "+appConfig.get('tabReglement')[i];
        }else{
          a.innerHTML = "<i class=\"fas fa-file-pdf\"></i> "+i;
        }
        li.appendChild(a);
        ul.appendChild(li);
      }
      section.appendChild(ul);
      self.sidebarContent.append(section);
    }
    

  }


  /**
   * @param {string} parcel
   */
  _contructor.prototype.updateLayerInfo = function(layerName, info){
    var self = this;
    self.abstracts[layerName] = info;
  }
  /**
   * @param {[]} data
   */
  _contructor.prototype.update = function(data){
    var self = this;
    self.open();
    if(data.type === 'niveaux'){
      self.updateNiveaux(data.value);
    }
    else if(data.type === 'parcel'){
      self.updateParcel(data.value);
    }
    else if(data.type === 'commune_info'){
      self.updateCommuneInfo(data.value);
    }
    else if(data.type === 'documents'){
      self.updateDocuments(data.value);
    }
    else {
      console.error((data ? data.type : ""), " not implemented yet in sidebar" )
    }
  }

  return _contructor;

})();
