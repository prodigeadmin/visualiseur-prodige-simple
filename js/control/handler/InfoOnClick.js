
function InfoOnClick(prodigeMap, number){
  'use strict'
  
  ////////// variable privée ///////////////////
  var _prodigeMap = prodigeMap;
  var _map = _prodigeMap ? _prodigeMap.map : null;
  var _vectorLayer = new OpenLayers.Layer.Vector('InfoOnClick_'+Date.now(), {
    styleMap: new OpenLayers.StyleMap({
        //fillColor: "#89b189",
        fillOpacity : 0,
        strokeColor : "#0e6d0e",
        strokeWidth : 3
    }) 
  });
  var _promiseNewData = $.Deferred();

  ////// Fonction publique ///////////////////////
  
  /** */
  function activate(){
    if(_map){
      _map.events.register("click", _map, onClick);
      $(this).data('count', 0);
      _map.events.register("touchmove", _map, onTouchMove);
      _map.events.register("touchstart", _map, onTouchStart);
      _map.events.register("touchend", _map, onTouchEnd);
    }
  }

  /** */
  function deactivate(){
    if(_map){
      _map.events.unregister("click", _map, onClick);
      
      _map.events.unregister("touchmove", _map, onTouchMove);
      _map.events.unregister("touchstart", _map, onTouchStart);
      _map.events.unregister("touchend", _map, onTouchEnd);
    }
  }

  /** 
   * @param {Parcel} parcel
   */
  function searchParcelInfo(parcel){
    
    $('#sidebar-info').hide();
    $('#sidebar-pdf').hide();
    var defer = $.Deferred();
    var params = null;
    var childNodes = getChildNodesByField();
    var layer = findLayerWithField(childNodes, Parcel.getConst("parcelChampName"));

    if(layer && parcel.id && parcel.boundStr){
      params = performQueries(null, [layer], parcel.boundStr);
    }

    if(params){
      getAdvancedAttribute(layer,  Parcel.getConst('idChampName'), parcel.id,[ Parcel.getConst("parcelChampName"), Parcel.getConst("villeChampName"), Parcel.getConst("sectionChampName")], true)
      .then(function(result){
        return getParcelFromRequest(result, layer, childNodes);
      })
      .then(function(result){
        var niveaux = getNiveauFromRequest(result);
        _promiseNewData.notify({type:"niveaux", value:niveaux});
        var communes_info = getCommuneInfoFromRequest(result);
        _promiseNewData.notify({type:"commune_info", value:communes_info});
        var documents = getDocumentsFromRequest(result);
        _promiseNewData.notify({type:"documents", value:documents});
        $('#sidebar-info').show();
        $('#sidebar-pdf').show();
        defer.resolve("ok");
      })
      .catch(function(error){
        console.error(error)
        defer.reject(error);
      })
    }
    else{
      defer.reject("ko");
    }

    return defer;
  }

  /** 
   * Promise pour notifier les informations après un click sur la carte
   * 
   * @return promise
   */
  function getPromiseOnClick(){
    return _promiseNewData.promise();
  }

  function onTouchStart(event){
    event.preventDefault();   
    
  }
  function onTouchMove(event){
    event.preventDefault();
    $(this).data('count', $(this).data('count')+1);
    
  }
  function onTouchEnd(event){
    event.preventDefault();    
    //ugly way but not possible do to better with OL
    if($(this).data('count') <= 3){
      // HERE YOUR CODE TO EXECUTE ON TAP-EVENT  
      onClick(event);
    }
    $(this).data('count', 0);
    
  }
       
  ///////// Fonction privée ////////////////////////
  /** */
  function onClick(position){
    
    $('#sidebar-info').hide();
    $('#sidebar-pdf').hide();
    var params = null;
    
    var childNodes = getChildNodesByField();
    var layer = findLayerWithField(childNodes, Parcel.getConst("parcelChampName"));

    if(layer){
      params = performQueries(position, [layer], null, null, [Parcel.getConst('idChampName'), Parcel.getConst("villeChampName")] );
      params.param.withGeom = true;
    }

    //
    if(params){
      getInfoRequest(params)
      .then(function(response){
        return getGeomFromRequest(response, layer, childNodes);
      })
      .then(function(response){
        var niveaux = getNiveauFromRequest(response);
        _promiseNewData.notify({type:"niveaux", value:niveaux});
         var communes_info = getCommuneInfoFromRequest(response);
        _promiseNewData.notify({type:"commune_info", value:communes_info});
        var documents = getDocumentsFromRequest(response);
        _promiseNewData.notify({type:"documents", value:documents});
        $('#sidebar-info').show();
        $('#sidebar-pdf').show();
      })
      
      .catch(function(error){
        console.error(error)
      });

     
    }

  
  }

  /** 
   * @return {Array.<Object>}
   */
  function getChildNodesByField(){
    var childNodes = [];
    var layerTree  = _prodigeMap.layerTreeManager.getLayerTree();

    if(layerTree && layerTree.childNodes && layerTree.childNodes instanceof Array){
      layerTree.childNodes.forEach(function(layer){
        childNodes = childNodes.concat(layer.childNodes);
      })
    }

    return childNodes;
  }

  /**
   * 
   * @param {Parcel} parcel 
   */
  function getParcelFromRequest(result, layer, childNodes){
    var  defer =  $.Deferred();

    try {
      result = JSON.parse(result);

      defer = getGeomFromRequest(result, layer, childNodes);
    } catch (error) {
      console.error(error);
      defer.reject("ko getParcelFromRequest");
    }
    return defer;
  }
  /** */
  function getCommuneInfoFromRequest(response){
    var result = new Object();
    if(response && response.results){
      var key = null;
      var data = response.results;
      for(key in data){
        if(data[key].fields && data[key].fields instanceof Array && data[key].data && data[key].data instanceof Array ){
          
          for (var indice in data[key].fields){
            if (data[key].fields[indice] === Parcel.getConst('ial_rl_1') ||
                    data[key].fields[indice] === Parcel.getConst('ial_rl_2') ||
                    data[key].fields[indice] === Parcel.getConst('ial_rn_1') ||
                    data[key].fields[indice] === Parcel.getConst('ial_rn_2') ){
              
              result[data[key].fields[indice]] = data[key]["data"][0][indice].replace(/\n|\r/g,'');
            }
          }
          
        }
      }
    }

    return result;
  }
  
  function getDocumentsFromRequest(response){
    
    var result = new Object();
    if(response && response.results){
      var key = null;
      var data = response.results;
      for(key in data){
        if(data[key].fields && data[key].fields instanceof Array && data[key].data && data[key].data instanceof Array ){
          for (var cle in data[key].data){
            var res = (data[key].data[cle]);
            for (var i in res){
              if(res[i] instanceof Object && typeof  (res[i].href) !== "undefined" && typeof(res[i].text)!== "undefined" && res[i].text!=""){
                result[data[key].fields[i]] = res[i].href + res[i].text;
              }     
            }
          }
          
        }
      }
    }
    return result;
  }
  

  /** */
  function getNiveauFromRequest(response){
    var result = [];
    if(response && response.results){
      var key = null;
      var data = response.results;
      for(key in data){
        if(data[key].fields && data[key].fields instanceof Array && data[key].data && data[key].data instanceof Array ){
          var idx = data[key].fields.findIndex(function(field){
            return ( field === Parcel.getConst('niveauChampName') );
          });

          var values = {layer: key, values: []};
          data[key].data.forEach(function(data){
            if(data[idx]){
              values.values.push(data[idx]);
            }
          });

          result.push(values);
        }
      }
    }

    return result;
  }

  /** */
  function getGeomFromRequest(response, layer, childNodes){
    var d =  $.Deferred();
    var result = response;
    var key = null;
    var keyExtent = null;
    var data = null;

    // recherche de l'index correspondant au field geom
    if(result && result.results &&  result.results[ layer.attributes.layerName ] && result.results[ layer.attributes.layerName ].fields && result.results[ layer.attributes.layerName ].fields instanceof Array ){
      key = result.results[ layer.attributes.layerName ].fields.findIndex(function(field){
        return (field === 'geom');
      });

      keyExtent = result.results[ layer.attributes.layerName ].fields.findIndex(function(field){
        return (field === 'extent');
      });

     
      data = result.results[ layer.attributes.layerName ].data;
      data = ( data instanceof Array && data.length > 0 ) ? data[0] : null;
      
      var parcels = Parcel.fromLayerRequest(result.results[ layer.attributes.layerName ]);
      if(parcels && parcels instanceof Array && parcels.length > 0) {
        _promiseNewData.notify({type:"parcel", value: parcels[0]});
      }
    } 
    else{
      d.reject("field 'geom' not found in response");
    }

    // on récupère la geomtrie
    if(key > -1 && data && data instanceof Array && data[key] ){
      keyExtent = (keyExtent === -1) ? key+1 : keyExtent;
      var extent = data[keyExtent] ?  data[keyExtent] : result.results[ layer.attributes.layerName ].data.extent;

      var params = performQueries(null, childNodes, data[key], 'polygon');
      showSelectFeatures(data[key], extent.replace("(", "").replace(")", ""));
      return getInfoRequest(params);
    }
    else{
      d.reject("geomtry data not found in response");
    }
   
    return d.promise();
  }

  /** */
  function positionToBounds(position) {
    var bounds = null;
    if (position instanceof OpenLayers.Bounds) {
      var minXY = _map.getLonLatFromPixel(new OpenLayers.Pixel(position.left, position.bottom));
      var maxXY = _map.getLonLatFromPixel(new OpenLayers.Pixel(position.right, position.top));
      
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,maxXY.lon, maxXY.lat);
    } 
    else if(typeof(position.type)!=="undefined" && position.type=="touchend"){
      var tol = 1;
      var minXY = _map.getLonLatFromPixel(new OpenLayers.Pixel(position.xy.x-tol, position.xy.y-tol));
      var maxXY = _map.getLonLatFromPixel( new OpenLayers.Pixel(position.xy.x+tol, position.xy.y+tol));
      
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,maxXY.lon, maxXY.lat);
      
    }
    else { 
      var tol = 1;
      var minXY = _map.getLonLatFromPixel(new OpenLayers.Pixel(position.layerX-tol, position.layerY-tol));
      var maxXY = _map.getLonLatFromPixel( new OpenLayers.Pixel(position.layerX+tol, position.layerY+tol));
      
      bounds = new OpenLayers.Bounds(minXY.lon, minXY.lat,maxXY.lon, maxXY.lat);
    }

    return bounds;
  }

  /**
   * 
   * @param {Object} position 
   * @param {Object} nodes 
   * @param {string} shape 
   * @param {string} shapeType 
   * @param {Array.<string>} optionalChamp
   * 
   * @return {Object} renvoies les Paramètres pour la requette prodige getinformation
   */
  function performQueries(position, nodes, shape, shapeType, optionalChamp) {
    var params = null;
    var msNodes =  {};
    var mapfile = appConfig.get('mapfile');

    nodes.forEach(function(node){
      if (!(node.attributes.OlLayer instanceof Carmen.Layer.WMSGroup) && appConfig.get('mapfile')) { 
        msNodes[mapfile] = ( msNodes[mapfile] &&  msNodes[mapfile]  instanceof Array ) ?  msNodes[mapfile] : [];
        msNodes[mapfile].push(node);    
      }
    })
      

    if(msNodes[mapfile].length > 0){
      params = ( buildParams(msNodes[mapfile], position, null, mapfile, shape, shapeType, optionalChamp) );
    }
  
    return params;
  }

  /** */
  function buildField(field, fieldKey, baseQueryURLList){
    var lists = Carmen.Util.chompAndSplit(field,';');
    var data = [];
    if(lists && lists instanceof Array){
      lists.forEach(function(list){
        data.push( Carmen.Util.decodeCarmenFieldDesc(list, baseQueryURLList[fieldKey]) );
      })
    }

    return data;
  }
  
  /** */
  function fieldFilter(field, fieldKey, baseQueryURLList, name){
    var lists = Carmen.Util.chompAndSplit(field,';');
    var retour = false;
    if(lists && lists instanceof Array){
      retour = !!(lists.find(function(list){
        var data = Carmen.Util.decodeCarmenFieldDesc(list, baseQueryURLList[fieldKey]);
        return (data && (data.name === name) );
      }));
    }

    return retour;
  }

  /** */
  function findLayerWithField(nodes, name){
    var layer = null;
    if(nodes && nodes instanceof Array){
     
      layer = nodes.find(function(node, fieldKey){
        if(node.attributes.infoFields){
          return fieldFilter(node.attributes.infoFields, fieldKey, node.attributes.baseQueryURL, name);
        }

        return false;
      });
    }

    return layer;
  }

  /** */
  function buildParams(nodes, position, fieldsFilter, mapfile, shape, shapeType, optionalChamp) {
    var nodesIndex = {};
    var layerNames = [];
    var fieldsList = [];
    var briefFieldsList = [];
    var baseQueryURLList = [];
    var fieldsDesc = [];
    var briefFieldsDesc = [];

    if( !(nodes && nodes instanceof Array) ){
      return null;
    }

    nodes.forEach(function(node, index){
      if(node.attributes.infoFields){
        nodesIndex[node.attributes.layerName] = index;
        layerNames.push(node.attributes.layerName);

        if( node.attributes.briefFields ){ 
          briefFieldsList.push(node.attributes.briefFields);
        }

        fieldsList.push(node.attributes.infoFields);
        baseQueryURLList.push(node.attributes.baseQueryURL);
      }
    });
  

    fieldsList.forEach(function(field, fieldKey){
      var data = buildField(field, fieldKey, baseQueryURLList);
      if(data.length > 0){
        fieldsDesc.push(data);
      }
    });
  

    briefFieldsList.forEach(function(field, fieldKey){
      var data = buildField(field, fieldKey, baseQueryURLList);
      if(data.length > 0){
        briefFieldsDesc.push(data);
      }
    });

    var objectField = [];

    if(optionalChamp && optionalChamp instanceof Array){
     
      optionalChamp.forEach(function(field){
        objectField.push(
          {"name":field,"alias":field,"type":"TXT","url":null}
        )
      });
    }
   
    fieldsDesc.forEach(function(field, index){
      fieldsDesc[index] = field.concat(objectField);
    });
    
    briefFieldsDesc.forEach(function(field, index){
      briefFieldsDesc[index] = field.concat(objectField);
    });

    var data = { 
      param : {
        layers : JSON.stringify(layerNames),
        fields : JSON.stringify(fieldsDesc),
        briefFields :JSON.stringify(briefFieldsDesc),
        shape : shape ? shape : Carmen.Util.positionToStr(positionToBounds(position)),
        shapeType:  shapeType ? shapeType : 'box',
        map : mapfile,
        showGraphicalSelection : true
      },
      layers: layerNames
    }

    return data;
  }

  /** */
  function getInfoRequest(param){

    $('#bodySpiner').show();
    // Requette ajax
    var promise = request.postByProxy( appConfig.get('getInformation') + "/" +param.layers.join('/') , param.param)
    
    promise.catch(function(error){
      console.error(error);
    })
    .always(function(){
      $('#bodySpiner').hide();
    })

    return promise;
  }
  
  /**
   * 
   * @param {Object} layer 
   * @param {string} champ 
   * @param {string | number} value 
   * @param {Array.<string>} optionalChamp 
   * 
   * @return {Deferred} Promise jquery
   */
  function getAdvancedAttribute(layer, champ, value, optionalChamp, withGeom){
    var fields = [champ];
    var objectField = [];
    
    if(optionalChamp && optionalChamp instanceof Array){
      fields = fields.concat(optionalChamp);
    }

    fields.forEach(function(field){
      objectField.push(
        {"name":field,"alias":field,"type":"TXT","url":null}
      )
    });

    var param = {
      layer: layer.attributes.layerName,
      qstring: "[" + champ + "]  = '" + value + "'",
      qOgrString: ( "(" + champ + ")  = '" + value  + "'"),
      pattern: value,
      op: "=",
      qitem: champ,
      fields: JSON.stringify(objectField),
      map: appConfig.get("mapfile"),
      withGeom: withGeom
    }

    $('#bodySpiner').show();
    // Requette ajax
    var promise  = request.postByProxy( appConfig.get('getAdvancedAttributes') , param)
    
    promise.catch(
      function(error){
        console.error(error);
      }
    )
    .always(function(){
      $('#bodySpiner').hide();
    });

    return promise
  }

  /**
   * 
   * @param {string} geom 
   * @param {string} bounds 
   */
  function showSelectFeatures(geom, bounds) {
    if(!geom){
      return null;
    }

    // layer pour la selection
    if ( !_map.getLayersByName(_vectorLayer.name).length ){
      _map.addLayer(_vectorLayer);
      _map.setLayerIndex(_vectorLayer, 10000);
    }

    // zoom sur la selection
    var tabExtent = bounds ?  bounds.split(",") : null;
    if(tabExtent && tabExtent instanceof Array && tabExtent.length === 4){
      _map.zoomToExtent(tabExtent);
    }

    // ajout du layer au vector de selection
    _vectorLayer.destroyFeatures();
    var wktFormat = new OpenLayers.Format.WKT();
    var features = wktFormat.read(geom);
    
    if(features) {
      _vectorLayer.addFeatures(features);
    }
    
    
  }

  //////////////////// Fonctions publiques //////////////////
  return {
    activate: activate,
    deactivate: deactivate,
    getPromiseOnClick: getPromiseOnClick,
    searchParcelInfo: searchParcelInfo
  }
};

