/**
 * 
 * @param {string} buttonPdfId 
 * @param {string} modalId 
 * @param {object} context 
 * @param {Sidebar} sidebar 
 * @param {Array.<string>} mapColor 
 */
function ExportPdf(buttonPdfId, modalId, context, sidebar, mapColor ){
  'use strict'
  var _buttonPdf = $("#"+buttonPdfId);
  var _modalId  = $("#"+modalId);
  var _pdfUrlId  = $("#"+modalId+"-url");

  var _context = context;
  var _sidebar = sidebar;

  var _mapColor = mapColor;
  
  var _isOk = true;

  var _metadataId = null;
  
  if( !util.checkElement(_buttonPdf, buttonPdfId ) || !util.checkElement(_modalId, modalId ) || !util.checkElement(_pdfUrlId, modalId+"-url" ) ){
    _isOk = false;
  }

  if(_isOk && (!_context || !_context.jsOws )){
    _isOk = false;
    console.error('context or context.jsOws is null');
  }

  if(_isOk && ( !_context.layer || !(_context.layer instanceof Array ) || _context.layer.length === 0) ){
    _isOk = false;
    console.error('no layer in context');
  }

  if(_isOk && ( !_sidebar || !sidebar instanceof Sidebar) ){
    _isOk = false;
    console.error('sidebar is null');
  }
  
  //_metadataId = _context.layer["0"].Extension.GEONETWORK_METADATA_ID;

  function init(){
    if(!_isOk){
      return null;
    }
    
    // demande d'export pdf
    _buttonPdf.on('click', function(){
        
      if( _sidebar.parcel ){
        _modalId.modal('show');
        _pdfUrlId.empty();
        _pdfUrlId.text('génération du document ...');

        request.post(appConfig.get('exportPdfUrl'), {jsContext:   _context.jsOws, parcel: _sidebar.parcel , niveaux: _sidebar.niveaux, commune_infos: _sidebar.commune_info, mapColor: _mapColor, metadataId: _metadataId  }).then(
          function(result){
            try {
              result = JSON.parse(result);
              if(result.url){
                _pdfUrlId.html('<a href="'+result.url+'" target="_blank">voir le document</a>');
              }
            } catch (error) {
              _pdfUrlId.text('Erreur pendant la génération du document');
              console.error(error);
            }
          },
          function(error){
              _pdfUrlId.text('Erreur pendant la génération du document');
            console.error(error);
          }
        )
      }
    });
  }

  return {
    init: init
  }
}
