 var ZoomToPlace = (function(){
  'use strict'

  /**
   * 
   * @param {string} elementRootId 
   * @param {Array.<Object>} configs
   */
  function _contructor(elementRootId, configs, map){
    var self = this;  

    /**  */
    var _elementRootId = null;

    /** Element où seront insérée les composants de recherche*/
    var _rootElement = null;

    /**  */
    var _configs = null;
    
    /** */
    var _combos = {};

    /**  carte openlayer */
    var _map = null;

    /** Extent utiliser pour le zoom sur la carte*/
    var _extent = null;

    Object.defineProperty(this, "elementRootId",{
      get: function() {
        return _elementRootId; 
      },
      set: function(id) { 
        _elementRootId = id; 
        _rootElement = document.getElementById(_elementRootId);
      },
      enumerable: true
    });
    
    Object.defineProperty(this, "configs",{
      get: function() {
        return _configs; 
      },
      set: function(configs) { 
        if(configs && configs instanceof Array){
          _configs = configs; 
        }
      },
      enumerable: true
    });

    Object.defineProperty(this, "combos",{
      get: function() {
        return _combos; 
      },
    });

    Object.defineProperty(this, "rootElement",{
      get: function() {
        return _rootElement; 
      },
      enumerable: false
    });

    Object.defineProperty(this, "map",{
      get: function() {
        return _map; 
      },
      set: function(map) { 
        if(map && map instanceof OpenLayers.Map){
          _map = map; 
        }
      },
      enumerable: true
    });

    Object.defineProperty(this, "extent",{
      get: function() {
        return _extent; 
      },
      set: function(extent) { 
        _extent = extent; 
      },
      enumerable: true
    });
   
    this.elementRootId = elementRootId;
    this.configs = configs;
    this.map = map;
  };

  ///////////////// Fonctions  ///////////////////////////
  _contructor.prototype.clearComboById = function(id){
    var self = this;
    if(self.combos[id] && self.combos[id].el){
      $(self.combos[id].el).empty() ;
      self.combos[id].options = [];
    }
  }
  
  /**
   * @return {Parcel}
   */
  _contructor.prototype.getParcel = function(){
    var self = this;

    /** On récupère l'id de la parcelle selectionnée */
    var combo = Object.values(self.combos).find(function(combo){
      return (combo.config && combo.config.labelText === Parcel.getConst('parcellesTitle') && combo.el && ( combo.el.selectedIndex !== 0) );
    });
    var pacelId = (combo && combo.el && (!combo.el.disabled)) ? combo.el.value : null;
  

    /** On récupère les coordonnées de la parcelle */
    var tabExtent = self.extent.split(",");
    if (tabExtent && tabExtent instanceof Array && tabExtent.length === 4) {
      tabExtent.forEach(function(strNumber, index){
        tabExtent[index] = parseInt(strNumber, 10); 
      })
    }

    return new Parcel(pacelId, tabExtent, self.extent );
    
  };

  _contructor.prototype.configComboListener = function(id){
    var self = this;
    
    if( (!self.combos[id]) || ( !(self.combos[id].childrenIds && self.combos[id].childrenIds instanceof Array))) {
      return false;
    }

    self.combos[id].el.addEventListener("change", function(){
          
      var index = self.combos[id].el.value;
      self.extent = self.combos[id].options[index] ? self.combos[id].options[index].config.extent : null;

      self.combos[id].childrenIds.forEach(function(childrendId){
        if( self.combos[childrendId] && self.combos[childrendId].el){
          self.updateOptionCombo(childrendId, self.combos[id].el.value, self.combos[childrendId].config.fieldNameId);

        }
      })
    });

    return true;
  }
 
  /**
   * 
   * @param {number} id index de _combo
   * @param {number} filterId id de la sélection parente 
   */
  _contructor.prototype.updateOptionCombo = function(id, filterId, filterItem){
    var self = this;

    if(!self.combos[id]){
      return false;
    }
    var param = self.combos[id].config.providerBaseParams;
    // Gestion des paramètres
    param.map = appConfig.get('mapfile');
    param.queryMode = 'gateData';
    param.queryParams_pk = 0;

    if(filterId && filterItem){
      param.filter_id = filterId;
      param.filter_item = filterItem;
    }
    
    $('#comboSpiner').show();
    request.getByProxy(appConfig.get('getArea'), param)
    .then(
      function(result){
        if(result && result.Names && result.Names instanceof Array){
          self.clearComboById(id);
          self.updateComboById(id);

          result.Names.forEach(function(value){
            var option = document.createElement("option");
            option.text = value.name;
            option.value = value.id;

            self.combos[id].el.add(option);
            self.combos[id].options[value.id] = {config: value};
            
          });
          
          self.combos[id].el.disabled = false;
        }
      },
      function(error){
        console.error(error);
      }
    )
    .always(function(){
      $('#comboSpiner').hide();
    });

    return true;
  }

  /**
   * 
   * @param {Element} element 
   */
  _contructor.prototype.addElToInterface = function(element){
    if(this.rootElement){
      this.rootElement.appendChild(element);
    }
  }

  /**
   * 
   * @param {number} id 
   */
  _contructor.prototype.updateComboById = function(id){
    var self =  this;

    if(!self.combos[id]){
      return false;
    }

    var option = document.createElement("option");
    option.text =   self.combos[id].config.labelText;
    option.selected = true;
    option.disabled = true;

    self.combos[id].el.add(option);
  

    return true;
  } 

  /**
   * 
   * @param {Object} configs 
   * @param {number} number 
   * @return {Element}
   */
  _contructor.prototype.buildCombo = function(config, number) {
    var self = this;

    // Div 
    var divFormGroup = document.createElement("div");
    $(divFormGroup).addClass( "form-group" );

    // Label
    var label = document.createElement("label");
    label.innerHTML = config.labelText;

    // Le combo
    var combo =  document.createElement("select");
    $(combo).addClass("form-control");

    self.combos[number] = {el: combo, parentId: (parseInt(config.linkedCombo, 10)-1), childrenIds: [] , config: config, options:{}};

    self.updateComboById(number);

    // ajout à la div
    divFormGroup.appendChild(label);
    divFormGroup.appendChild(combo);

    return divFormGroup;
  }

  /**
   * 
   */
  _contructor.prototype.buildInterface = function(){
    var self = this;
    if(!self.configs){
      return false;
    }

    self.configs.forEach(function(config, index){
      // building combo
      var combo = self.buildCombo(config, index);
      self.addElToInterface(combo);

      // ajouts des options ou désactivation des combos lier 
      
      if(!isFinite(self.combos[index].parentId) || self.combos[index].parentId === null){
        self.updateOptionCombo(index);
      }
      else{
        self.combos[self.combos[index].parentId].childrenIds.push(index);
        self.combos[index].el.disabled = true;  
      }

      // gestion des events
      self.configComboListener(index);
    });

    return true;
  }

  /**
   * @function
   */
  _contructor.prototype.zoomOnMap = function(){
    var self = this;
    var tabExtent = self.extent.split(",");
    if(tabExtent && tabExtent instanceof Array && tabExtent.length === 4){
      self.map.zoomToExtent(tabExtent);
    }
    $('#modalRecherche').modal('hide');
  }

  /////////////////// Prototype ///////////////////////

  return _contructor;
})();