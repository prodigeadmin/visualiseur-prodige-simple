var Parcel = (function(){
  'use strict'
  var _transtype = {};


  /**
   * 
   * @param {number | string} id 
   * @param {Array.<number>} bound
   * @param {String} boundStr 
   */
  function _contructor(id, bound, boundStr, numParcel, city, section ){
    if(!_transtype){
      console.error('transtype is null');
    }
    var self = this;


    self.id = id;
    self.bound = bound;
    self.boundStr = boundStr;
    self.numParcel = numParcel;
    self.city = city;
    self.section = section;
  }

  /**
   * @param {string} name
   * @return {any}
   */
  _contructor.getConst = function(name){
    return appConfig.get(name);
  }

  /**
   * création d'un object Parcel à partir d'un layer d'une requette prodige comme getInformation ou getArea
   * @param {object} layer
   * @return {Parcel} 
   */
  _contructor.fromLayerRequest = function(layer){
    var parcels = [];
    var extent = []; 
    var extentStr = "";


    if(! (layer && layer.data && layer.fields && layer.data instanceof Array && layer.fields instanceof Array) ){
      return null;
    }

    var fitler = {};
    // on filtre les champs qu'on veut récupérer
    layer.fields.forEach(function(field, idx){
      if( _transtype[field] ){
        fitler[idx] =  _transtype[field] ;
      }
    });
    
    if(layer.extent){
      extentStr = layer.extent.replace("(", "").replace(")", "");
      extent = extentStr.split(",");
    }

    // on associe les valeurs
    layer.data.forEach(function(datas){
      var parcel = new _contructor();
      datas.forEach(function(data, index){ 
        if(fitler[index]){
          parcel[fitler[index]] = data;
        }

        parcel.bound = extent;
        parcel.boundStr = extentStr;
      });

      if(parcel.id){
        parcels.push(parcel);
      }
    })
   
    return parcels;
  }

  /**
   * 
   */
  _contructor.updateTranstype = function(){
    _transtype[appConfig.get('idChampName')]     = 'id';
    _transtype[appConfig.get('villeChampName')]  = 'city';
    _transtype[appConfig.get('parcelChampName')] = 'numParcel';
    _transtype[appConfig.get('sectionChampName')] = 'section';
  }
  

  return _contructor;
})();
