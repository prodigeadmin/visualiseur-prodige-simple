var application = (function(){
  'use strict'

  ////// Variable privée ////////////////
  /** */
  var _prodigeMap = null;

  /** */
  var _map  = null;

  /** */
  var _context = null;

  /** */
  var _sidebar = null;

  /** */
  var _infoOnClick = null;

  /** */
  var _zoomToPlace = null;

  /** */
  var _mapColor = null;

  ////////// Fonction /////////////////////
  /** */
  function createMap() {
    var d = $.Deferred();
    _prodigeMap = new Prodige.Map({
      renderTo : "prodigeviewer",
      fileUrl : appConfig.get('context'),
      owscontext : appConfig.get('context'),
      legendVisible :   true ,
      keymapVisible :  false ,
      defaultControl : 'Navigation',
      legendRenderer : 'advanced'
    },
      function(isOk){
        isOk ? d.resolve('ok') :  d.reject('ko');
      }
    );

    return d.promise();
  }

  /** */
  function initControl(layerColor){
    // Zoom sur une parcelle
    _zoomToPlace = new ZoomToPlace('modalRechercheCombos', generateCtrlConfig.toZoom(_context), _map );
    _zoomToPlace.buildInterface();

    // Sidebar
    _sidebar = new Sidebar('sidebar', _prodigeMap, layerColor);
    _sidebar.updateTreeLayer();
    layerInfo.getRequest(_context, _sidebar);

    // Info click
    _infoOnClick = new InfoOnClick(_prodigeMap, 31);
    _infoOnClick.activate();

    var promise = _infoOnClick.getPromiseOnClick();
    promise.progress(function(data){
      _sidebar.update(data);
    });

    // Recherche d'une parcelle
    var button = document.getElementById('modalRechercheSubmit');
    if(button){
      button.addEventListener('click', function(){ onSearchParcel(button) });
    }

    // gestion d'export pdf
    var exportPdf = new ExportPdf("sidebar-pdf", "modal-pdf", _context, _sidebar, _mapColor );
    exportPdf.init();
    
  }

  function onSearchParcel(button){
    _zoomToPlace.zoomOnMap();
    button.disabled = true;
    var parcel = _zoomToPlace.getParcel();

    _infoOnClick.searchParcelInfo(parcel)
    .always( function(){ button.disabled = false; });
  }

  function onPageReady(){
    Prodige.loadJsFiles(false);
    Prodige.onReady(function(){
      var p1 = createMap();

      var p2 = mapColor.getLayerColor();

      $.when(p1, p2).then(
        function(p1Result, color){
          _context = new Carmen.JSONContext(_prodigeMap.owscontext.jsOws);
          _map = _prodigeMap.map;
          _mapColor = color;

          Parcel.updateTranstype();

          initControl(color);
        }
      )
      .catch(function(error){
        console.error(error);
      })
    });
  }

  /////////// Fonctions et variables publique ///////////////////////////
  return {
    onPageReady: onPageReady
  }
})();

/** Main */
$(function() {
  $('#comboSpiner').hide();
  $('#bodySpiner').hide();
  
  appConfig.updateFromServer().then(function(){
    application.onPageReady();
  })
})