Visualiseur cartographique PRODIGE simplifié
=============

Visualiseur géographique basé sur PRODIGE (https://gitlab.adullact.net/prodigeadmin/Prodige).  
Ce visualiseur permet la consultation d'une carte PRODIGE dans un visualiseur simplifié, conçu en responsive design, offrant les fonctions suivantes :
* navigation,
* localisation dans des listes de localisation (3 critères)
* gestion des couches visibles, légende spécifique
* sélection d'objets et informations
* affichage de liens dynamiques relatifs aux objets sélectionnés
* export PDF spécifique

Ce visualiseur a été conçu pour répondre au besoin de création du site Information Acquéreur Locataire de Mayotte (https://www.information-acquereurs-locataires-mayotte.fr/), réalisation de la DEAL Mayotte.  


Pré-requis 
-----------
* Le visualiseur doit être installé sur une plateforme PRODIGE.  
* PRODIGE doit être en version 4.1.  
* installation de nodejs 10 LTS ou supérieur (https://nodejs.org/)
* installation de wkhtmltopdf (https://wkhtmltopdf.org/) (pour export PDF)
* installation de ghostscript (https://www.ghostscript.com) (pour export PDF)


Déploiement
-----------
Le déploiement se passe en six étapes :
1. cloner le projet dans le dossier de votre choix
2. copier ce dossier dans le dossier /var/www/ ( ou autres dossiers de déploiement Apache)
3. lancer la commande npm install dans le dossier racine (dépendance node 10 LTS ou supérieur)
3. créer une carte sur la plateforme PRODIGE
4. configurez le fichier config_prod.php
5. personnalisez la présentation dans les répertoires template, css

Fichier config_prod.php
------------------
Le fichier de configuration permet le paramétrage du visualiseur selon les spécificités de la carte.

### Exemple
```
 // Mapfile et context
define('MAPFILE_PATH', '/home/prodige/cartes/Publication/');
define('MAPFILE', MAPFILE_PATH."ial.map");
define('CONTEXT_FILE', "/home/prodige/owscontext/1_ial.ows");

// URL 
define('URL_METADATA_INFO',"https://www.prodige41.alkante.com/geonetwork/srv/fre/q");
define('URL_CARTO_PRODIGE', 'https://carto.prodige41.alkante.com');
define('CARMEN_URL_SERVER_DATA', 'https://datacarto.prodige41.alkante.com');

// PATH pour les export PDF
define("PATH",  __DIR__."/");
define('UPLOAD', PATH."upload");

// Template html pour le pdf
define('TEMPLATE_PATH', __DIR__."/template/aleas-pdf.html");
define('PDF_FICHE_PATH', '/home/prodige/cartes/METADATA/ial_cerfa/');
define('PDF_POSTFIXE_FICHE', "_cerfa.pdf");

$UPLOAD_PATHS  = array(
  'img' =>    UPLOAD.'/image',
  'log' =>    UPLOAD.'/log',
  'pdf' =>    UPLOAD.'/pdf',
  'html' =>   UPLOAD.'/html',
);

// défintion des champs d'une parcel
$PARCEL_CHAMP_NAME = array(
  "parcelChampName"    => "num_parcel",
  "parcellesTitle" => "Parcelles",
  "sectionChampName" => "lb_section",
  "niveauChampName" => "lb_niveau",
  "ial_rl_1" => "ial_rl_t1", 
  "ial_rl_2" => "ial_rl_t2",
  "ial_rn_1" => "ial_rn_t1",
  "ial_rn_2" => "ial_rn_t2",
  "idChampName"     => "id_obj",
  "villeChampName"  => "lb_com",
  "sectionsTitle" => "Sections"
);

define('layerParcelle',  "layer8");

$tabReglement = array(
  "l_arr_ap_rn" => "Arrêté d'approbation PPRN",
  "l_ar_ap_rn"=> "Arrêté d'approbation PPRN",
  "l_arr_ap_rl" => "Arrêté d'approbation PPRL",
  "l_pres_rn" => "Note de présentation PPRN",
  "l_pres_rl" => "Note de présentation PPRL",
  "l_reg_rn" => "Réglement PPRN",
  "l_reg_rl" => "Réglement PPRL",
  "l_z_r_rn_1" => "Zonage réglementaire PPRN planche 1",
  "l_z_r_rn_2" => "Zonage réglementaire PPRN planche 2",
  "l_z_r_rn_3" => "Zonage réglementaire PPRN planche 3",
  "l_z_r_rn_4" => "Zonage réglementaire PPRN planche 4",
  "l_z_r_rl_1" => "Zonage réglementaire PPRL planche 1",
  "l_z_r_rl_2" => "Zonage réglementaire PPRL planche 2",
  "l_z_r_rl_3" => "Zonage réglementaire PPRL planche 3",
  "l_z_r_rl_4" => "Zonage réglementaire PPRL planche 4",
  "notice_ial" => "Notice IAL",
  "note_pprl"  => "PAC Submersion Marine et RTC",
  "arrt_pprl"  => "Arrêté de prescription du PPRL",
  "l_arr_ial"  => "Arrêté IAL"
);

define('PRODIGE_API_VERSION', '4.1');
```

### Carte et contexte
* MAPFILE_PATH : chemin des mapfiles
* MAPFILE : chemin du mapfile de la carte métier
* CONTEXT_FILE : chemin du fichier de contexte au format ows (fichier généré par le composeur PRODIGE)

#### URL
* URL_METADATA_INFO : URL d'accès à l'API geonetwork
* URL_CARTO_PRODIGE : URL d'accès au visualieur PRODIGE

#### Données temporaires générées
* UPLOAD : chemin des données générées (droits d'écriture apache nécessaires)

#### Template PDF
* TEMPLATE_PATH : chemin d'accès au template smarty du PDF généré
* PDF_FICHE_PATH : chemin d'accès aux fichiers PDF concaténés (l'application concatène un fichier PDF dépendant de la sélection et un PDF de la carte)
* PDF_POSTFIXE_FICHE : suffixe des fichiers PDF concaténés (le radical correspond au nom de la commune sans espaces et accents) 

#### Identfiant de sélection
* layerParcelle : nom de la couche de sélections dans le mapfile (MAP/LAYER/NAME)

#### Paramétrage des sélections
* parcelChampName : nom du champ portant le libellé de sélection
* parcellesTitle : libellé associé aux entités de sélection
* sectionChampName : nom du champ portant la sélection intermédiaire de niveau n-1
* sectionsTitle : libellé associé au champ portant la sélection intermédiaire (niveau n-1)
* idChampName : nom du champ portant la sélection intérmédiaire de niveau n-2
* villeChampName : libellé associé au champ portant la sélection intermédiaire (niveau n-2)
* niveauChampName : nom du champ niveau utilisé pour les analyses thématiques dans les couches
* ial_rl_1 : texte personnalisé utilisé dans le template d'export PDF
* ial_rl_2 : texte personnalisé utilisé dans le template d'export PDF
* ial_rn_1 : texte personnalisé utilisé dans le template d'export PDF
* ial_rn_2 : texte personnalisé utilisé dans le template d'export PDF

#### liens externes
* tabReglement : champs présentant des liens et texte associés (les champs doivent être présents dans la table et interrogeables)

#### version API PRODIGE
* version majeure de PRODIGE installée

Personnalisation des textes et styles
------------------
La personnalisation des textes s'effectue dans le fichier index.html  
La personnalisation des exports consiste à modifier le fichier template/aleas-pdf.html (mise en forme de la page de génération de la carte) et template/layer.html (mise en forme de la partie correspondant à chaque carte intégrée)  
La personnalisation des styles peut être faire dans css/styles.css (interface générale) et css/pdf.css (feuille de style associée aux exports PDF)