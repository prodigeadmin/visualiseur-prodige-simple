<?php

class Util {
    /**
    * Do a CURL call
    * @param $method   GET|POST|PUT|PATCH|DELETE
    * @param $url      url (query params should be passed directly in the url, such as ...?key=value)
    * @param $postData data in case of a post|put|patch|delete call
    * @param $bJson    force json content-type, postData should be a json string in that case; false by default
    * 
    * @return the curl response
    */
  static public function doCurlCall($method, $url, $postData=null, $bJson=false, $headerCustom, $user="", $password="")
  {
    $method = strtoupper($method);

    $postData = is_array($postData) && !empty($postData) ?  json_encode($postData) : $postData;
    
    $result = false;
    $ch = curl_init($url);

    /*** SSL auto-signé */
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FAILONERROR, true);

    if($user != "" && $password != ""){
      curl_setopt($ch, CURLOPT_USERPWD, "$user:$password");
    }

    switch ($method) {
      case 'POST':
      case 'PUT':
      case 'PATCH':
      case 'DELETE':
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        break;
    }
    $header = array();;

    if ($bJson && $postData!=null) {
      $header = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($postData) /* TODO what if $postData is not a string! */
      );
    
    }
    
    if(!empty($headerCustom)){
      $header = array_merge($headerCustom, $header);
    }

    if(!empty($header)){
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    };

    $result = curl_exec($ch);

    if( $result === false ) {
      
      throw new \Exception('CURL Error: '.curl_error($ch));
      curl_close($ch);
    }

    curl_close($ch);

    return $result;
  }
}