<?php

require_once(__DIR__."/../../config.php");

class MapserverUtil{
  
  /**
   * generateImg from MapObj
   * 
   * @param mapObj $oMap
   * @param paths array of paths
   * @param mapExtent map Extent
   * @param parcelId selection identifier
   * @param with log true to add logs
   *
   * @return imgFile
   */
  static function generateImg($oMap, $paths, $mapExtent, $parcelId, $withLog = false){
    $rand = time()."_".rand(0,10000);
    
    $layer = @$oMap->getLayerByName(layerParcelle);
    if($layer){
        $qitem = "id_obj";
        $qstring = "([id_obj]='".$parcelId."')";
        $res = @$layer->queryByAttributes($qitem, ($qstring) ,MS_MULTIPLE)==MS_SUCCESS;
    }
    $oMap->setExtent($mapExtent->minx-10,$mapExtent->miny-10, $mapExtent->maxx+10, $mapExtent->maxy+10);
    $img = $oMap->drawQuery();

    //si l'échelle est inférieure au 2000, on revient au 2000
    if($oMap->scaledenom <2000){

        $point = ms_newPointObj();
        $point->setXY(intval($oMap->width/2), intval($oMap->height/2));
        $oMap->zoomscale(2000, $point, $oMap->width, $oMap->height, $mapExtent) ;

        $img = $oMap->drawQuery();
    }

    $mapfileToImg = MAPFILE_PATH."/TMP_PDF_".$rand.".map";
    $imgfile = $oMap->web->imagepath."/map_".$rand. "." . "png";
    
    $img->saveImage($imgfile);
    CSCarteMetadata_addScaleInMap($oMap, $imgfile);
    return $imgfile;
  }


  /**
   * get array of map layers
   *
   * @param mapObj $oMap
   * @param string $item
   *
   * @return layerObj
   */
  static function getLayersToItems($oMap, $item){
    $numLayer = $oMap->numlayers;
    $layers = array();
    for($i = 0; $i < $numLayer ; $i++) {
      $layer = $oMap->getLayer($i);
      if( $layer->classitem === $item){
        $layers[] = $layer;
      }
    }

    return $layers;
  }

  /**
   * load context from jsContext and mapfile
   * @param string $strJsContext string json de context
   * @param string $mapfile : path to mapfile
   * @param integer $width : pixel width of the map to create
   * @param integer $height : pixel width of the map to create
   * @param integer $mapWidth : pixel width of the navigator map
   * @param integer $exportType : 0 conservation de l'échelle, 1 conservation de l'emprise
   * @return mapObj
   */
  static function loadContext($strJsContext, $mapfile, $width, $height, $mapWidth, $exportType=0){
    $oMap = @ms_newMapObj($mapfile);
    if (is_null($oMap)) {
      return false;
    }
    
    //first of All set print size
    $oMap->setSize($width, $height);
    if(!$strJsContext){
      return $oMap;
    }

    $oContext = json_decode($strJsContext, false);
    
    if(!$oContext) {
      return $oMap;
    }
    
    $xmin = $oContext->ViewContext->General->BoundingBox->attributes->minx;
    $ymin = $oContext->ViewContext->General->BoundingBox->attributes->miny;
    $xmax = $oContext->ViewContext->General->BoundingBox->attributes->maxx;
    $ymax = $oContext->ViewContext->General->BoundingBox->attributes->maxy;
    
    if($exportType === 1){//emprise conservée en largeur, adaptation en hauteur, là où les variations sont moins importantes
      
      //calcul du centre
      $Xcenter = ($xmax + $xmin)/2;
      $Ycenter = ($ymax + $ymin)/2;
      $ratio = ($xmax - $xmin)/$width ;

      $ymin = $Ycenter-$ratio*$height/2;
      $ymax = $Ycenter+$ratio*$height/2;
      
    }
    else {//echelle conservée
      ///calcul du ratio en largeur
      $ratio = ($xmax - $xmin)/$mapWidth ;
      //calcul du centre
      $Xcenter = ($xmax + $xmin)/2;
      $Ycenter = ($ymax + $ymin)/2;
      //affectation des extents
      $xmin = $Xcenter-$ratio*$width/2;
      $xmax = $Xcenter+$ratio*$width/2;
      $ymin = $Ycenter-$ratio*$height/2;
      $ymax = $Ycenter+$ratio*$height/2;
    }

    $metadata = $oContext->ViewContext->General->Extension;
    foreach($metadata as $key=>$value){
      if ( is_object($value) ) continue; 
      if ( is_array($value) ) continue; 
      if ( in_array($key, array("MaxBoundingBox")) ){
          continue;
      }
      $key = str_replace(array("mapSettings_"), "", $key);
      $key = str_replace(array("tool_"), "", $key);
      $key = strtoupper($key);
      $value = urldecode($value);
      $oMap->setMetadata($key, $value); 
    }
    
    //load Extent
    
    $oMap->setExtent($xmin,$ymin, $xmax, $ymax);

    //load Layers params
    $Layerlist = $oContext->ViewContext->LayerList->Layer;
    $tabDrawingLayer = array();
    foreach($Layerlist as $idLayer => $oLayer){
      //set LayersOrder
      $layerName = $oLayer->attributes->name;
      $tabDrawingLayer[]= $layerName;
      $oMapservLayer = @$oMap->getLayerByName($layerName);
      if(!$oMapservLayer){
          
        // WMS layer added by user...
        if (isset($oLayer->Extension->wms_layer)) {
          $oMapservLayer = ms_newLayerObj($oMap);
          $oMapservLayer->set("name", "layer".$idLayer);
          $oMapservLayer->set("type", MS_LAYER_RASTER);
          $oMapservLayer->setConnectionType(MS_WMS);
          $oMapservLayer->set("connection", $oLayer->Extension->wms_onlineresource);
          $oMapservLayer->set("status", MS_ON);
          $oMapservLayer->setMetaData("wms_srs", $oLayer->Extension->wms_srs);
          $oMapservLayer->setMetaData("wms_name", $oLayer->Name);
          //$oMapservLayer->setMetaData("wms_title", $params[3]);
          $oMapservLayer->setMetaData("wms_format", $oLayer->Extension->wms_format);
          $oMapservLayer->setMetaData("wms_onlineresource",  $oLayer->Extension->wms_onlineresource);
          $oMapservLayer->setMetaData("wms_server_version", "1.1.1");
        }
        // KML Layer drawn by user...
        else if (isset($oLayer->Document)) {
          $tmp_kml_filename = tempnam(CARMEN_PATH_CACHE, "kml_") . ".kml";
          $oMapservLayer = drawKmlLayer($oMap, 
            $tmp_kml_filename, 
            $oLayer->Document, MS_SHAPE_POLYGON, 
            $oLayer->Extension->layerSettings_olStyle);
        }
        else { //WFS added Layer
          //var_dump($oLayer->Server);
          $mapUrl = $oLayer->Server->OnLineResource->attributes->url;
          $parsedUrl = parse_url($mapUrl);
          $var  = explode('&', $parsedUrl["query"]);
          foreach($var as $val)
          {
            $x = explode('=', $val);
            $arr[$x[0]] = $x[1];
          }
          if(file_exists($arr["map"])){
            $oMapCopy = ms_newMapObj($arr["map"]);
            if($oMapCopy){
              $oLayerCopy = $oMapCopy->getLayerByName($oLayer->attributes->name);
              $oMapservLayer = ms_newLayerObj($oMap, $oLayerCopy);
              // need to redefine data path in an absolute way
              $newDataLocation = dirname($arr["map"]) . "/" . $oMapservLayer->data;
              $oMapservLayer->set("data", $newDataLocation);
            }
          }
        }
      }
      else {
        // in case of WMS-C, tuiled server url is replaced by no tuiled reference server 
        // cos mapserver does not handled tuiled server
        if ($oMapservLayer->getMetaData("WMTS_WMS_ONLINERESOURCE")!="") {
          $onlineRessource = rawurldecode($oMapservLayer->getMetaData("WMTS_WMS_ONLINERESOURCE"));
          $oMapservLayer->set("connection", $onlineRessource);
          $oMapservLayer->setMetaData("wms_onlineresource", $onlineRessource);
          $wmsLayer = rawurldecode($oMapservLayer->getMetaData("WMTS_WMS_LAYER"));
          $oMapservLayer->setMetaData("wms_name", $wmsLayer);
        }
        if (isset($oLayer->Extension->wmsc_wms_onlineresource)) {
          $onlineRessource = rawurldecode($oLayer->Extension->wmsc_wms_onlineresource);
          $oMapservLayer->set("connection", $onlineRessource);
          $oMapservLayer->setMetaData("wms_onlineresource", $onlineRessource);
        }
        if (isset($oLayer->Extension->wmsc_wms_layer)) {
          $wmsLayer = rawurldecode($oLayer->Extension->wmsc_wms_layer);
          $oMapservLayer->setMetaData("wms_name", $wmsLayer);
        }
        
        //since mapserver does not support 1.3.0 version for the moment
        //warn on http://trac.osgeo.org/mapserver/ticket/3039
        //todo verify where is the problem client / server
        if($oMapservLayer->getMetaData("wms_server_version")=="1.3.0"){
          $oMapservLayer->setMetaData("wms_server_version", "1.1.1");
        }
        
      }
    //turn status off/on
      if($oLayer->attributes->hidden == 1) {
        $oMapservLayer->set("status", MS_OFF);
      }else{
        $oMapservLayer->set("status", MS_ON);
      }
    }
    $oMap->setlayersdrawingorder($tabDrawingLayer);
    
    $layerTree = $oContext->ViewContext->General->Extension->LayerTree;
    $asArray = function($tree)use(&$asArray){
      if ( is_object($tree) ){
        $temp = array();
        foreach(get_object_vars($tree) as $var_name=>$var_value){
          $temp[$var_name] = $asArray($var_value);
        }
        $tree = $temp;
      }
      else if ( is_array($tree) ){
        foreach($tree as $var_name=>$var_value){
          $tree[$var_name] = $asArray($var_value);
        }
      }
      return $tree;
    };
    $applyOpacity = function (&$opacities, $tree) use(&$applyOpacity) {
      foreach ($tree as $key=>$node){
        if ( isset($node["attributes"]) && $node["attributes"]["type"]=="group" && isset($node["LayerTreeNode"]) ){
          $applyOpacity($opacities, $node["LayerTreeNode"]);
        }
        if ( isset($node["attributes"]) && $node["attributes"]["type"]=="layer" ){
          $opacities[$node["attributes"]["mapName"]] = $node["attributes"]["opacity"]*100;
        }
      }
    };
    $layerTree = $asArray($layerTree);

    $opacities = array();
    $applyOpacity($opacities, $layerTree);
    
    $oMap->setlayersdrawingorder((array_keys($opacities)));
    $status = array();
    for($iLayer=0; $iLayer<$oMap->numlayers; $iLayer++){
        $oLayer = $oMap->getLayer($iLayer);
        if ( $oLayer ) {
            $status[$oLayer->name] = $oLayer->status;
            $oLayer->set('status', MS_DELETE);
        }
    }
    $tempMapfile = str_replace(".map", "_".uniqid("tmp").".map", $mapfile);
    $oMap->save($tempMapfile);
    
    $oMapTemp = ms_newMapObj($tempMapfile);
    foreach (array_reverse($opacities, true) as $layerName=>$layerOpacity){
        $oLayer = $oMap->getLayerByName($layerName);
        if ( $oLayer ){
            $oLayerTemp = ms_newLayerObj($oMapTemp, $oLayer);
            if ( $oLayerTemp ){
                $oLayerTemp->set("status", $status[$oLayer->name]);
                $oLayerTemp->set("opacity", intval($layerOpacity));
            }
        }
    }

    @unlink($tempMapfile);
    $oMap = $oMapTemp;
    
    $index = 0;

    if(isset($oContext->ViewContext->General->Extension->Annotation)){
      $annot_config = $oContext->ViewContext->General->Extension->Annotation; 
      
      $annot_file_polygon = CARMEN_PATH_CACHE."/annotation_".rand(0,1000).".kml";
      drawKmlLayer($oMap, 
        $annot_file_polygon, 
        $annot_config[0], 
        MS_SHAPE_POLYGON, 
        $annot_config[3]);

      $annot_file_line = CARMEN_PATH_CACHE."/annotation_".rand(0,1000).".kml";
      drawKmlLayer($oMap, 
        $annot_file_line, 
        $annot_config[1], 
        MS_SHAPE_LINE, 
        $annot_config[3]);

      $annot_file_point = CARMEN_PATH_CACHE."/annotation_".rand(0,1000).".kml";
      drawKmlLayer($oMap, 
        $annot_file_point, 
        $annot_config[2], 
        MS_SHAPE_POINT, 
        $annot_config[3]);
    }
    
    if(isset($oContext->ViewContext->General->Extension->Label)){
      $label_config = $oContext->ViewContext->General->Extension->Label; 
      $tmp_kml_label_filename = tempnam(CARMEN_PATH_CACHE, "kml_label") . ".kml";
      drawKmlLayer($oMap, 
        $tmp_kml_label_filename, 
        $label_config->data, MS_LAYER_POINT, 
        $label_config->style
      );
    }

    return $oMap;
  }

}