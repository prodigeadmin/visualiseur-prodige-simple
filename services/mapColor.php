<?php
include_once(__DIR__."/../config.php");

$oMap = null;
if(is_file(MAPFILE)){
  $oMap = ms_newMapObj(MAPFILE);

  $layerColor = array();
}


if(!is_null($oMap)){
  $numLayer = $oMap->numlayers;

  for($i = 0; $i < $numLayer ; $i++) {
    $layer = $oMap->getLayer($i);
    if( $layer->classitem === $PARCEL_CHAMP_NAME['niveauChampName']){
      $numClass = $layer->numclasses;
      for($j = 0; $j < $numClass; $j++){
        $class = $layer->getClass($j);
        $numStyle = $class->numstyles;
        for($k =0; $k < $numStyle; $k++){
          $style = $class->getStyle($k);

          if($style->color && $style->color->red  > -1 && $style->color->green > -1 && $style->color->blue > -1){
            $color = sprintf("#%02x%02x%02x", $style->color->red, $style->color->green, $style->color->blue );
            $layerColor[$layer->name][$class->name] = $color; 
          }
        }
      } 
    }
  }

  exit( json_encode($layerColor, true) ); 
}
else{
  echo "error not mapfile found\n";
}