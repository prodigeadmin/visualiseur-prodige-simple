<?php
require_once(__DIR__."/../config.php");

$response = $PARCEL_CHAMP_NAME;
$response['mapfile'] = MAPFILE;
$response['urlProdige'] = URL_CARTO_PRODIGE;
$response['urlMetadataInfo'] = URL_METADATA_INFO;

$response['CARMEN_URL_SERVER_DATA'] = CARMEN_URL_SERVER_DATA;
$response['PRODIGE_API_VERSION'] = PRODIGE_API_VERSION;
$response['tabReglement'] = $tabReglement;

exit(json_encode($response));