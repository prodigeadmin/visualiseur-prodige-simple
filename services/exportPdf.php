<?php
require_once(__DIR__."/../config.php");
require_once(__DIR__."/class/mapserverUtil.php");
require_once(__DIR__."/class/util.php");
require_once(__DIR__."/lib_draw.php");

/**
 * Export DPF class
 */
class ExportPDf{
  private $jsContext;
  private $parcel;
  private $niveaux;
  private $mapColor;  
  private $path;
  private $paths;
  private $oMap; 
  private $layersCtx;
  private $layers;
  private $dom; 
  
  /**
   * Constructor
   * @param mapfile : path to $mapfile
   * @param path : path to app dir
   * @param uploadPaths : array of upload paths
   * @param selectFieldName : select field name
   *
   */
  function __construct($mapfile, $path, $uploadPaths, $selectFieldName){
    
    $this->jsContext  = $_POST["jsContext"];
    $this->parcel     = $_POST["parcel"];
    $this->niveaux    = isset($_POST["niveaux"])  ? $_POST["niveaux"] : array();
    $this->mapColor   = $_POST["mapColor"];
    $this->metadataId = $_POST["metadataId"];
    
    $this->communes_info = $_POST["commune_infos"];

    $this->niveaux = $this->niveaux ?  $this->niveaux  : array();
    $this->mapColor =  $this->mapColor ?  $this->mapColor: array();


    $this->path = $path;
    $this->paths = $paths;

    if(!$this->jsContext || !$this->parcel){
      header("HTTP/1.0 404");
      echo "not found";
      exit();
    }

    $this->oMap = MapserverUtil::loadContext(json_encode($this->jsContext), $mapfile, 300, 300, 1900, 1 );
    $this->layersCtx = $this->getLayerFromContext();
    $this->layers = MapserverUtil::getLayersToItems($this->oMap, $selectFieldName['niveauChampName'] );

    $this->dom = new DOMDocument();
  }

  function run(){
    // chargement du template html
    $mapElement = $this->loadTemplate();
    
    // Chargement template layert
    $this->writeHtml( file_get_contents($this->path."template/layer.html") );

    // mise à jour de l'image
    $mapExtent = ms_newRectObj();
    
    if(isset($this->parcel['bound']) && is_array($this->parcel['bound']) && count($this->parcel['bound']) == 4){
        $mapExtent->setExtent($this->parcel['bound'][0], $this->parcel['bound'][1], $this->parcel['bound'][2], $this->parcel['bound'][3]);
    }
    //génération image globale 300*300
    $imgfile = MapserverUtil::generateImg($this->oMap, $this->paths, $mapExtent, $this->parcel['id']);
    $this->setImgMapElement($mapElement, $imgfile );

    //génération images au détail
    $toDeletes = $this->updateImgSrc($mapExtent);

    // Ajout des infos
    $this->writeInfo();

    // on sauve le html et on export en pdf
    $htmlFile = $this->paths["html"].'/tmp_'.time()."_".rand(0,10000).".html";
    $this->dom->saveHTMLFile($htmlFile);

    $pdfPath = $this->removeAccents(PDF_FICHE_PATH. $this->parcel['city'].PDF_POSTFIXE_FICHE);

    $pdfUrl = $this->getPdfFromHtml($htmlFile, $pdfPath)."\n";
  
    $toDeletes[] = $htmlFile;
    $toDeletes[] = $imgfile;

    $this->deleteTmpFile($toDeletes);

    return $pdfUrl;
  }

  /**
   * replace accents in string
   * @param str : string
   */
  function removeAccents($str) {
    $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
    $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
    return str_replace($a, $b, $str);
  }

  /**
   * tranform html to pdlf
   * @param string $htmlPath : html path file
   * @param string pdfPath : pdf path file
   * @return pdf path
   */

  function getPdfFromHtml($htmlPath, $pdfPath){
    $path = "";
    if(isset($this->parcel['id']) && isset($this->paths['pdf'])){
      // Sauvegarde du html
      /** Conversion du html en pdf  */ 
      $filename =  "aleas_".$this->parcel['id'].".pdf";
      $footerText = "Etat des servitudes 'risques' et d'information sur les sols concernant la parcelle ".$this->parcel['section'].$this->parcel['numParcel']." / ".$this->removeAccents($this->parcel['city'])."";
      
      $nameTmp1 = "tmp1_".$filename;
      $nameTmp2 = "tmp2_".$filename;
      
      $fileTmp1 = sys_get_temp_dir()."/".$nameTmp1;
      $fileTmp2 =    sys_get_temp_dir()."/".$nameTmp2;
      $path =  $this->paths['pdf']."/".$filename;

      $cmd = 'wkhtmltopdf --print-media-type --orientation "Portrait" --margin-left "10"';
      $cmd .= ' --margin-right "10" --margin-top "10" --margin-bottom "10" --page-size "A4" --zoom "1"';
      $cmd .= ' --title "'.$filename.'"';
      $cmd .= ' --disable-smart-shrinking '.$htmlPath.' '.$fileTmp1;

      exec($cmd);

      $output = array();
      
      exec('gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE='.$fileTmp2.' -dBATCH '.$pdfPath.'  '.$fileTmp1.' ', $output) ;
 
      exec('gs -o '.$path.' -sDEVICE=pdfwrite -c "<</EndPage {2 ne {0.5 setgray /Times 10 selectfont 30 10 moveto ('.$footerText.')'.
            'show pop true}{pop false} ifelse} >> setpagedevice" -f '.$fileTmp2);
  
      $path = str_replace($this->path, "", $path);
    }

    return $path;
  }

  /**
   * load HTML template
   */
  function loadTemplate(){
    $strHtml = file_get_contents(TEMPLATE_PATH);
    $strHtml = str_replace('%ial_rl_1%', $this->communes_info["ial_rl_t1"], $strHtml);
    $strHtml = str_replace('%ial_rl_2%', $this->communes_info["ial_rl_t2"], $strHtml);
    $strHtml = str_replace('%ial_rn_1%', $this->communes_info["ial_rn_t1"], $strHtml);
    $strHtml = str_replace('%ial_rn_2%', $this->communes_info["ial_rn_t2"], $strHtml);
    $this->dom->loadHTML($strHtml);

    $mapElement = $this->dom->getElementById('map-image');

    if ( $mapElement->hasAttribute("width") && $mapElement->hasAttribute("height")){
      $width = $mapElement->getAttribute("width");
      $height = $mapElement->getAttribute("height");
      $this->oMap->setSize($width, $height);
    } else {
      $this->oMap->setSize(800, 500);
    }
    
    $width = $this->oMap->width;
    $height = $this->oMap->height;

    return $mapElement;
  }

  /**
   * create image Element
   * @param DOMElement $mapElement
   * @param string $imgFile
   *
   * @return void
   */
  function setImgMapElement($mapElement, $imgfile){
    $mapElement->setAttribute("src", $imgfile);
    $f = 'imagecreatefrom'.strtolower("png");
    $img = $f($imgfile);
    $mapElement->setAttribute("width", intval(imagesx($img)/2));
    $mapElement->setAttribute("height", intval(imagesy($img)/2));
  }

  /**
   * remove tmp files
   */
  function deleteTmpFile($files){
    foreach($files as $file){
      exec('rm '.$file);
    }
  }

  /**
   * return layers from context
   */
  function getLayerFromContext(){

    $layers = array();
    if(isset($this->jsContext['ViewContext']['LayerList']['Layer'])){
      foreach($this->jsContext['ViewContext']['LayerList']['Layer'] as $layer){
        if(isset($layer['attributes']['name'])){
          $layers[$layer['attributes']['name']] = $layer;
        }
      }
    }
    return $layers;
  }

  /**
   * create detailled image
   * @param id : id html content
   * @param layerName : layer name in mapfile
   *  
   */
  function writeBadgeNiveau($id , $layerName){
    if( !isset($this->niveaux[  $layerName] ) ){
      return false;
    }
    $xpath = new DOMXpath($this->dom);
    $badgeHtml = '<span class="badge " style="background: %COLOR%; color: white" >%NIVEAU%</span> ';
    $badgeElement = $xpath->query("//*[@id='".$id."']")->item(0);

    foreach($this->niveaux[  $layerName ] as $niveau){
      $color = isset($this->mapColor[$layerName][$niveau]) ? $this->mapColor[$layerName][$niveau] : "#777"  ;
      $tmpStr = str_replace('%NIVEAU%', $niveau, $badgeHtml);
      $tmpStr = str_replace('%COLOR%', $color, $tmpStr);

      $f = $this->dom->createDocumentFragment();
      $f->appendXML($tmpStr);

      $badgeElement->appendChild($f);
    }

    return true;
  }

  /**
   * update HTML template
   * param layerContent : html content
   */
  function writeHtml( $layerContent ){
    $pair = 0;
    $content = "";
    $num = 0;
    foreach($this->layers as $key => $layer){
      if( isset($this->layersCtx[$layer->name]) && isset($this->niveaux[$layer->name])  ){
        $num++;
        $title = isset($this->layersCtx[$layer->name]['Title']) ? urldecode( $this->layersCtx[$layer->name]['Title'] ) : $layer->name;

        $idBadge = 'badge-'.strval($key);
        $tmpStr = str_replace('%TILTE%', $title, $layerContent);
        $tmpStr = str_replace('%ID-BADGE%', $idBadge, $tmpStr);
        
        $break ="";
        if($num ==5){
          $break.=" break-before";
        }
       
        $content .= str_replace('%ID%', "layer-".$key, $tmpStr);

        if($pair === 1){
          $pair = 0;
          
          $f = $this->dom->createDocumentFragment();
          
          $f->appendXML('<div class="row">'.$content.'</div>');

          $contentElement = $this->dom->getElementById('content');
          $contentElement->appendChild($f);

          $content = "";
        }
        elseif($pair === 0){
          $pair = 1;
        }
      }

    
    }

    if($pair === 1){
      $f = $this->dom->createDocumentFragment();
      $f->appendXML('<div class="row'.$break.'">'.$content.'</div>');

      $contentElement = $this->dom->getElementById('content');
      $contentElement->appendChild($f);

      $content = "";
    }
  }

  /**
   * Update image in html
   * @param mapExtent : img extent
   */
  function updateImgSrc($mapExtent){
    $this->oMap->setSize(300, 300);
    $xpath = new DOMXpath($this->dom);
    
    $toDeletes = array();
    
    foreach($this->layers as $key => $layer){
      $layer->status = MS_OFF;
    }
    
    foreach($this->layers as $key => $layer){
      if( isset($this->layersCtx[$layer->name]) && isset($this->niveaux[$layer->name])  ){
        $layer->status = MS_ON;
        
        $imgElement = $xpath->query("//*[@id='layer-".$key."']")->item(0);
        
        $imgfile = MapserverUtil::generateImg($this->oMap, $this->paths, $mapExtent, $this->parcel['id'] );
        $toDeletes[] = $imgfile;

        $this->setImgMapElement($imgElement, $imgfile);
      
        $this->writeBadgeNiveau("badge-".$key, $layer->name );
      }
      $layer->status = MS_OFF;
    }

    return $toDeletes;
  }

  /**
   * Update map Extent
   */
  function updateMapExtent(){
    if(isset($this->parcel['bound']) && is_array($this->parcel['bound']) && count($this->parcel['bound']) == 4){
      $this->oMap->setExtent($this->parcel['bound'][0], $this->parcel['bound'][1], $this->parcel['bound'][2], $this->parcel['bound'][3]);
    }
  }

  /**
   * get Information from Metadata
   */
  function getInfo(){
    $result = Util::doCurlCall('GET',URL_METADATA_INFO."/".( IN_PROD ? $this->metadataId : METADATA_ID_DEBUG ), null, false, array('Accept:application/json') );
                  
    $result = iconv('UTF-8', 'UTF-8//IGNORE', utf8_encode($result)); 
    $result = json_decode($result, true);

    $title = "";  
    $abstract = "";

    if( isset($result['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:citation']['gmd:CI_Citation']['gmd:title']['gco:CharacterString']['#text']) ){
      $title = $result['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:citation']['gmd:CI_Citation']['gmd:title']['gco:CharacterString']['#text'];
    }

    if(isset($result['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:abstract']['gco:CharacterString']['#text'])){
      $abstract = $result['gmd:identificationInfo']['gmd:MD_DataIdentification']['gmd:abstract']['gco:CharacterString']['#text'];
    }

    return array('title' => $title, 'abstract' => $abstract ); 
  }

  /**
   * Write specific information in html
   */
  function writeInfo(){
    $info  = 'Commune de <strong>%CITY_NAME%</strong><br />
              Parcelle <strong>%PARCEL_NUMBER%</strong>';

    $infoDate = "Document imprimé le %DATE%";

    $infoParcel = $this->dom->getElementById("infoParcel");

    if($infoParcel){
      $tmpStr = str_replace('%CITY_NAME%', $this->parcel['city'], $info );
      
      $tmpStr = str_replace('%PARCEL_NUMBER%', $this->parcel['section'].$this->parcel['numParcel'], $tmpStr );

      $f = $this->dom->createDocumentFragment();
      $f->appendXML($tmpStr);

      $infoParcel->appendChild($f);
    }

    $footerDate = $this->dom->getElementById("footer-date");
    if($footerDate){
      
      $date = date("d/m/Y");

      $tmpStr = str_replace('%DATE%', (" ".$date), $infoDate);

      $f = $this->dom->createDocumentFragment();
      $f->appendXML($tmpStr);

      $footerDate->appendChild($f);
    }
  }
}

$exportPDf = new ExportPDf(MAPFILE, PATH, $UPLOAD_PATHS, $PARCEL_CHAMP_NAME );
$pdf = $exportPDf->run();

$response = array("url" => $pdf);
exit(json_encode($response));
