<?php

// Mapfile et context
define('MAPFILE_PATH', '/home/prodige/cartes/Publication/');
define('MAPFILE', MAPFILE_PATH."ial.map");
define('CONTEXT_FILE', "/home/prodige/owscontext/1_ial.ows");

// URL 
define('URL_METADATA_INFO',"https://www.observatoire-de-mayotte.fr/geonetwork/srv/fre/q");
define('URL_CARTO_PRODIGE', 'https://carto.observatoire-de-mayotte.fr');
define('URL_DATACARTO_PRODIGE', 'https://datacarto.observatoire-de-mayotte.fr');

// PATH pour les export PDF
define("PATH",  __DIR__."/");
define('UPLOAD', PATH."upload");

// Template html pour le pdf
define('TEMPLATE_PATH', __DIR__."/template/aleas-pdf.html");
define('PDF_FICHE_PATH', '/home/prodige/cartes/METADATA/ial_cerfa/');
define('PDF_POSTFIXE_FICHE', "_cerfa.pdf");

$UPLOAD_PATHS  = array(
  'img' =>    UPLOAD.'/image',
  'log' =>    UPLOAD.'/log',
  'pdf' =>    UPLOAD.'/pdf',
  'html' =>   UPLOAD.'/html',
);

// défintion des champs d'une parcel
$PARCEL_CHAMP_NAME = array(
  "parcelChampName"    => "num_parcel",
  "parcellesTitle" => "Parcelles",
  "sectionChampName" => "lb_section",
  "niveauChampName" => "lb_niveau",
  "ial_rl_1" => "ial_rl_t1", 
  "ial_rl_2" => "ial_rl_t2",
  "ial_rn_1" => "ial_rn_t1",
  "ial_rn_2" => "ial_rn_t2",
  "idChampName"     => "id_obj",
  "villeChampName"  => "lb_com",
  "sectionsTitle" => "Sections"
);

define('layerParcelle',  "layer8");

$tabReglement = array(
  "l_arr_ap_rn" => "Arrêté d'approbation PPRN",
  "l_ar_ap_rn"=> "Arrêté d'approbation PPRN",
  "l_arr_ap_rl" => "Arrêté d'approbation PPRL",
  "l_pres_rn" => "Note de présentation PPRN",
  "l_pres_rl" => "Note de présentation PPRL",
  "l_reg_rn" => "Réglement PPRN",
  "l_reg_rl" => "Réglement PPRL",
  "l_z_r_rn_1" => "Zonage réglementaire PPRN planche 1",
  "l_z_r_rn_2" => "Zonage réglementaire PPRN planche 2",
  "l_z_r_rn_3" => "Zonage réglementaire PPRN planche 3",
  "l_z_r_rn_4" => "Zonage réglementaire PPRN planche 4",
  "l_z_r_rl_1" => "Zonage réglementaire PPRL planche 1",
  "l_z_r_rl_2" => "Zonage réglementaire PPRL planche 2",
  "l_z_r_rl_3" => "Zonage réglementaire PPRL planche 3",
  "l_z_r_rl_4" => "Zonage réglementaire PPRL planche 4",
  "notice_ial" => "Notice IAL",
  "note_pprl"  => "PAC Submersion Marine et RTC",
  "arrt_pprl"  => "Arrêté de prescription du PPRL",
  "l_arr_ial"  => "Arrêté IAL"
);

define('PRODIGE_API_VERSION', '4.1');
 