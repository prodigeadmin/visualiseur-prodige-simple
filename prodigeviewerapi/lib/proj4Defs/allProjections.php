<?php
$mode = "local";
if ( isset($_GET["mode"]) ){
  $mode = $_GET["mode"];
}
header("Content-Type: text/javascript");

if ( file_exists(__DIR__.'/allProjections.js') ){
	readfile(__DIR__.'/allProjections.js');
	exit();
}

ob_start();
if ( $mode!="remote" ){
  $jsFiles = glob(__DIR__.'/*.js');
  foreach ($jsFiles as $jsFile){
    readfile($jsFile);
    echo "\n";
  }
	
} else {
  $epsgs = array(
    "EPSG:2154", 
    "EPSG:27561", 
    "EPSG:27562", 
    "EPSG:27563", 
    "EPSG:27564", 
    "EPSG:27571", 
    "EPSG:27572", 
    "EPSG:27573", 
    "EPSG:27574", 
    "EPSG:27581", 
    "EPSG:27582", 
    "EPSG:27583", 
    "EPSG:27584", 
    "EPSG:2970", 
    "EPSG:2971", 
    "EPSG:2972", 
    "EPSG:2973", 
    "EPSG:2975", 
    "EPSG:2980", 
    "EPSG:2987", 
    "EPSG:3035", 
    "EPSG:3163", 
    "EPSG:3169", 
    "EPSG:3170", 
    "EPSG:3171", 
    "EPSG:32301", 
    "EPSG:32620", 
    "EPSG:32630", 
    "EPSG:32631", 
    "EPSG:32632", 
    "EPSG:32738", 
    "EPSG:3296", 
    "EPSG:3297", 
    "EPSG:3312", 
    "EPSG:3727", 
    "EPSG:3942", 
    "EPSG:3943", 
    "EPSG:3944", 
    "EPSG:3945", 
    "EPSG:3946", 
    "EPSG:3947", 
    "EPSG:3948", 
    "EPSG:3949", 
    "EPSG:3950", 
    "EPSG:4171", 
    "EPSG:4258", 
    "EPSG:4326", 
//     "EPSG:4471", 
    "EPSG:4625", 
    "EPSG:4965"
  );
  foreach ($epsgs as $epsg){
    readfile("http://spatialreference.org/ref/".str_replace(":", "/", strtolower($epsg))."/Proj4jsjs/");
    echo "\n";
  }
} 
$content = ob_get_clean();
@file_put_contents(__DIR__.'/allProjections.js', $content);
echo $content;
exit();